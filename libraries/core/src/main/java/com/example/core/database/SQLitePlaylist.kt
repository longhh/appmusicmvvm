package com.example.core.database

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.core.model.Playlist
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class SQLitePlaylist @Inject constructor(@ApplicationContext val context: Context?) :
    SQLiteOpenHelper(context, "playlist.sqlite", null, 1) {


    private fun queryData(sql: String?) {
        val database = writableDatabase
        database.execSQL(sql)
    }

    private fun getData(sql: String?): Cursor? {
        val database = readableDatabase
        return database.rawQuery(sql, null)
    }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS Playlist (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME NVARCHAR(50))")
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {

    }

    @SuppressLint("Recycle")
    fun getDataPlaylist(): ArrayList<Playlist> {
        val arrPlaylist = ArrayList<Playlist>()
        val dataPlaylist: Cursor? = getData("SELECT * FROM Playlist")
        arrPlaylist.clear()
        if (dataPlaylist != null) {
            while (dataPlaylist.moveToNext()) {
                val id = dataPlaylist.getInt(0)
                val name = dataPlaylist.getString(1)
                arrPlaylist.add(Playlist(id, name))
            }
        }
        return arrPlaylist
    }

    fun insertDataPlaylist(name: String?) {
        queryData("INSERT INTO Playlist VALUES(null, '$name')")
        getDataPlaylist()
    }

    fun deleteDataPlaylist(id: Int?) {
        queryData("DELETE FROM Playlist WHERE ID = '$id'")
        getDataPlaylist()
    }

    fun updateData(id: Int?, name: String?) {
        queryData("UPDATE Playlist SET NAME = '$name' WHERE Id = '$id'")
        getDataPlaylist()
    }
}