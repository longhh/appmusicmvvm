package com.example.core.utils.listener

interface OnItemClickListener {
    fun setOnClick(position: Int)
}