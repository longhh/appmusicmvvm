package com.example.core.database

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.core.model.Playlist
import com.example.core.model.SongItem
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class SQLitePlaylistDetail @Inject constructor(@ApplicationContext val context: Context?) :
    SQLiteOpenHelper(context, "playlist_detail.sqlite", null, 1) {


    private fun queryData(sql: String?) {
        val database = writableDatabase
        database.execSQL(sql)
    }

    private fun getData(sql: String?): Cursor? {
        val database = readableDatabase
        return database.rawQuery(sql, null)
    }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS PlaylistDetail (ID INTEGER PRIMARY KEY AUTOINCREMENT" +
                ", NAME NVARCHAR(50), IMAGE NVARCHAR(200), SOURCE NVARCHAR(200), ARTIST NVARCHAR(50), DURATION INT(10))")
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {

    }

    @SuppressLint("Recycle")
    fun getDataPlaylistDetail(): ArrayList<SongItem> {
        val arrPlaylistDetail = ArrayList<SongItem>()
        val dataPlaylistDetail: Cursor? = getData("SELECT * FROM PlaylistDetail")
        arrPlaylistDetail.clear()
        if (dataPlaylistDetail != null) {
            while (dataPlaylistDetail.moveToNext()) {
                val id = dataPlaylistDetail.getInt(0)
                val name = dataPlaylistDetail.getString(1)
                val image = dataPlaylistDetail.getString(2)
                val source = dataPlaylistDetail.getString(3)
                val artist = dataPlaylistDetail.getString(4)
                val duration = dataPlaylistDetail.getInt(5)
                arrPlaylistDetail.add(SongItem(id, name, image, source, artist, duration))
            }
        }
        return arrPlaylistDetail
    }

    fun insertDataPlaylistDetail(name: String?, image: String?, source: String?, artist: String?, duration: Int?) {
        queryData("INSERT INTO PlaylistDetail VALUES(null, '$name', '$image', '$source', '$artist', '$duration')")
        getDataPlaylistDetail()
    }

    fun deleteDataPlaylist(id: Int?) {
        queryData("DELETE FROM PlaylistDetail WHERE ID = '$id'")
        getDataPlaylistDetail()
    }
}