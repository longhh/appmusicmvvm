package com.example.core.model

import com.google.gson.annotations.SerializedName

data class Banner(
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("image")
    var image: String? = "",
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("artist")
    var artist: String? = "",
    @SerializedName("imageArtist")
    var imageArtist: String? = ""
)