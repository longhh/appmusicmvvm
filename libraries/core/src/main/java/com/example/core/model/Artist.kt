package com.example.core.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Artist(
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("image")
    var image: String? = "",
    @SerializedName("id")
    var id: Int? = null
) : Serializable