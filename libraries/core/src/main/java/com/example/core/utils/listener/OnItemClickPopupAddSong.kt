package com.example.core.utils.listener

interface OnItemClickPopupAddSong {
    fun setOnClick(position: Int)

    fun setOnClickAddPlaylist(position: Int)

    fun setOnClickAddFavourite(position: Int)
}