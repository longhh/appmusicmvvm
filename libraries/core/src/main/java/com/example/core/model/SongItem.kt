package com.example.core.model

import java.io.Serializable

data class SongItem(
    var id: Int? = null,
    var name: String? = "",
    var image: String? = "",
    var source: String? = "",
    var artist: String? = "",
    var duration: Int? = null
) : Serializable
