package com.example.core.model

data class HomePage(
    var type: Int? = null,
    var banner: ArrayList<Banner>? = null,
    var album: ArrayList<Album>? = null,
    var artist: ArrayList<Artist>? = null,
    var genre: ArrayList<Genre>? = null

)