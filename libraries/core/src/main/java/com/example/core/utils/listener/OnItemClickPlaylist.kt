package com.example.core.utils.listener

interface OnItemClickPlaylist {
    fun deleteClick(position: Int)

    fun itemClick(position: Int)
}