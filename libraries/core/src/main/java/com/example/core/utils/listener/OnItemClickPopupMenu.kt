package com.example.core.utils.listener

interface OnItemClickPopupMenu {
    fun setOnClick(position: Int)

    fun setOnClickPopupMenu(position: Int)
}