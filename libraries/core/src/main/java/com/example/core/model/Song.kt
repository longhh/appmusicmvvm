package com.example.core.model

import com.google.gson.annotations.SerializedName

data class Song(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("albumId")
    var albumId: Int? = null,
    @SerializedName("artistId")
    var artistId: Int? = null,
    @SerializedName("genreId")
    var genreId: Int? = null,
    @SerializedName("source")
    var source: String? = "",
    @SerializedName("image")
    var image: String? = "",
    @SerializedName("duration")
    var duration: Int? = null
)