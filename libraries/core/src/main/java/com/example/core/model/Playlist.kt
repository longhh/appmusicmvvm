package com.example.core.model

data class Playlist(
    var id: Int? = null,
    var name: String? = null
)