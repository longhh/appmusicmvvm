package com.example.core.model

import java.io.Serializable

data class Users (
    var id: String? = null,
    var username: String? = null,
    var imageURL: String? = null
) : Serializable