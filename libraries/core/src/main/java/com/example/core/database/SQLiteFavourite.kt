package com.example.core.database

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.core.model.SongItem
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class SQLiteFavourite @Inject constructor(@ApplicationContext val context: Context?) :
    SQLiteOpenHelper(context, "favourite.sqlite", null, 1) {


    private fun queryData(sql: String?) {
        val database = writableDatabase
        database.execSQL(sql)
    }

    private fun getData(sql: String?): Cursor? {
        val database = readableDatabase
        return database.rawQuery(sql, null)
    }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS Favourite (ID INTEGER PRIMARY KEY AUTOINCREMENT" +
                ", NAME NVARCHAR(50), IMAGE NVARCHAR(200), SOURCE NVARCHAR(200), ARTIST NVARCHAR(50), DURATION INT(10))")
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {

    }

    @SuppressLint("Recycle")
    fun getDataFavourite(): ArrayList<SongItem> {
        val arrFavourite = ArrayList<SongItem>()
        val dataFavourite: Cursor? = getData("SELECT * FROM Favourite")
        arrFavourite.clear()
        if (dataFavourite != null) {
            while (dataFavourite.moveToNext()) {
                val id = dataFavourite.getInt(0)
                val name = dataFavourite.getString(1)
                val image = dataFavourite.getString(2)
                val source = dataFavourite.getString(3)
                val artist = dataFavourite.getString(4)
                val duration = dataFavourite.getInt(5)
                arrFavourite.add(SongItem(id, name, image, source, artist, duration))
            }
        }
        return arrFavourite
    }

    fun insertDataFavourite(name: String?, image: String?, source: String?, artist: String?, duration: Int?) {
        queryData("INSERT INTO Favourite VALUES(null, '$name', '$image', '$source', '$artist', '$duration')")
        getDataFavourite()
    }

    fun deleteDataFavourite(id: Int?) {
        queryData("DELETE FROM Favourite WHERE ID = '$id'")
        getDataFavourite()
    }
}