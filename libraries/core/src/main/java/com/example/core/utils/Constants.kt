package com.example.core.utils

import java.util.*

object Constants {
    const val PREF_FILE_NAME = "Preferences"
    const val DEFAULT_TIMEOUT = 30
    const val DURATION_TIME_CLICKABLE = 500

    const val ACTION_MUSIC = "ACTION_MUSIC"
    const val ACTION_MUSIC_FROM_RECEIVER = "ACTION_MUSIC_FROM_RECEIVER"

    const val CHANNEL_ID = "push_notification_id"

    object NetworkRequestCode {
        const val REQUEST_CODE_200 = 200    //normal
        const val REQUEST_CODE_400 = 400    //parameter error
        const val REQUEST_CODE_401 = 401    //unauthorized error
        const val REQUEST_CODE_403 = 403
        const val REQUEST_CODE_404 = 404    //No data error
        const val REQUEST_CODE_500 = 500    //system error
    }

    object ApiComponents {
        const val BASE_URL = "https://google.com"
    }

    object Bundle {
        const val BOOLEAN = "IsServiceRunning"
        const val ARTIST = "Artist"
        const val ALBUM = "Album"
        const val POSITION = "Position"
        const val DATA_FROM_SERVICE = "DataFromService"
        const val MEDIA_POSITION_FROM_SERVICE = "MediaPositionFromService"
        const val TIMER = "Timer"
        const val LIST_SONG = "List"
        const val GENRE = "Genre"
        const val SONG_ITEM = "SongItem"
        const val SONG_ITEM_FROM_CONTROL_BAR = "SongItemFromControlBar"
        const val USERS = "Users"
    }

    object Firebase {
        const val FIRE_BASE_DEFAULT = "default"
        const val FIRE_BASE_USERS = "Users"
        const val FIRE_BASE_ID = "id"
        const val FIRE_BASE_USERNAME = "username"
        const val FIRE_BASE_IMAGE_URL = "imageURL"
        const val REFERENCE_FAVOURITE = "Favourite"
    }

    object ActionIntent {
        const val LIST_SONG = "ListSong"
        const val SONG = "Song"
        const val SONG_UPDATE = "SongUpdate"
        const val BOOLEAN = "IsServiceRunning"
        const val MEDIA_POSITION = "MediaPositionUpdate"
        const val TIMER = "Timer"
    }
}
