package com.example.core.model

import java.io.Serializable

data class SongPersonal(
    var id: Long? = null,
    var name: String? = "",
    var image: String? = "",
    var source: String? = "",
    var artist: String? = "",
    var duration: Long? = null,
    var album: String? = "",
    var albumId: Long? = null
) : Serializable
