package com.example.core.network

import com.example.core.model.*
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface RetrofitService {
    @GET("https://6218ef9681d4074e859c7eb8.mockapi.io/api/v1/songs")
    suspend fun getSong(): ArrayList<Song>
    @GET("https://621ed50b849220b1fca26484.mockapi.io/api/v1/banner")
    suspend fun getBanner(): ArrayList<Banner>
    @GET("https://6215b273c9c6ebd3ce2f03ca.mockapi.io/albums")
    suspend fun getAlbum(): ArrayList<Album>
    @GET("https://621ed50b849220b1fca26484.mockapi.io/api/v1/artists")
    suspend fun getArtist(): ArrayList<Artist>
    @GET("https://6218ef9681d4074e859c7eb8.mockapi.io/api/v1/top")
    suspend fun getTrending(): ArrayList<Song>
    @GET("https://621f24a1311a70591401d374.mockapi.io/genrees")
    suspend fun getGenre(): ArrayList<Genre>


    companion object {
        var retrofitService: RetrofitService? = null

        fun getInstance(): RetrofitService {
            if (retrofitService == null) {
                val client = OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor())
                    .build()
                val retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(Gson()))
                    .client(client)
                    .baseUrl("https://6218ef9681d4074e859c7eb8.mockapi.io/api/v1/")
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }
}