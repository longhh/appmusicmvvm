package com.example.baseproject.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.core.utils.Constants

class Receiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val bundle = intent?.extras
        val actionMusic = bundle?.getInt(Constants.ACTION_MUSIC)
        val intentService = Intent(context, MediaService::class.java)

        val bundleFromReceiver = Bundle()
        if (actionMusic != null) {
            bundleFromReceiver.putInt(Constants.ACTION_MUSIC_FROM_RECEIVER, actionMusic)
            intentService.putExtras(bundleFromReceiver)
            context?.startService(intentService)
        }
    }
}