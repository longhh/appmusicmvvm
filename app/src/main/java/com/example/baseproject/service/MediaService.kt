package com.example.baseproject.service

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.*
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.container.MainActivity
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.io.*
import java.net.URL
import java.util.concurrent.ThreadLocalRandom

class MediaService : Service() {
    companion object {
        const val ACTION_PAUSE = 1
        const val ACTION_RESUME = 2
        const val ACTION_CLOSE = 3
        const val ACTION_NEXT = 4
        const val ACTION_BACK = 5
    }

    var favourite = false
    var isLooping = false
    var mediaPlayer: MediaPlayer? = null
    var isPlaying = true
    var isTimer = false
    var filename: String? = null
    var mediaPosition: Int? = null
    var isRandom = false
    var isPause = false
    private var setDataSource = false
    var isResume = false
    private var isServiceRunning = true
    var isStart = true
    var position = 0
    var timer = 0
    var arrSong: ArrayList<SongItem>? = null
    var song: SongItem? = null
    private var currentPosition = 0
    private val handler = Handler(Looper.getMainLooper())
    private var runnable = object : Runnable {
        override fun run() {
            if (mediaPlayer != null) {
                sendNotification(arrSong!![position])
                mediaPosition = mediaPlayer!!.currentPosition / 1000
                mediaPositionFromService(mediaPlayer!!.currentPosition / 1000)
                dataSongFromService(arrSong?.get(position))
                timer--
                timerFromService(timer)
                listSong(arrSong, position)
            }
            handler.postDelayed(this, 1000)
        }
    }

    inner class MediaBinder : Binder() {
        fun getMediaService(): MediaService = this@MediaService
    }

    private var binder = MediaBinder()

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        setDataSource = false
        val bundle = intent?.extras
        if (bundle != null) {
            if (isStart) {
                arrSong = bundle.getSerializable(Constants.Bundle.SONG_ITEM) as ArrayList<SongItem>?
                position = bundle.getInt(Constants.Bundle.POSITION)
                dataSong(arrSong!![position])
                filename = arrSong!![position].name
                startMusic(arrSong!![position])
                sendNotification(arrSong!![position])
                isStart = false
            }
            handleActionMusic(bundle.getInt(Constants.ACTION_MUSIC_FROM_RECEIVER))
        }
        return START_NOT_STICKY
    }

    fun startMusic(songItem: SongItem?) {
        val listFile: Array<String> = applicationContext.fileList()
        filename = songItem!!.name
        song = songItem
        mediaPlayer?.release()
        mediaPlayer = null
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer().apply {
                setAudioAttributes(
                    AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .build()
                )
                if (songItem.source?.startsWith("https://")!!) {
                    for (i in listFile.indices) {
                        val file = File(applicationContext.filesDir, filename.toString())
                        if (listFile[i] == filename) {
                            setDataSource(file.path)
                            prepare()
                            setDataSource = true
                        }
                    }
                    if (!setDataSource) {
                        val uri: Uri = Uri.parse(songItem.source)
                        setDataSource(applicationContext, uri)
                        prepare()
                        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
                            cacheMusic(songItem)
                        }
                    }
                } else {
                    val uri: Uri = Uri.parse(songItem.source)
                    setDataSource(applicationContext, uri)
                    prepare()
                }

                setOnPreparedListener {
                    start()
                }
            }
            handler.postDelayed(runnable, 1000)
            dataSong(arrSong!![position])
            isPlaying = true
        }
    }

    private fun cacheMusic(songItem: SongItem?) {
        val url = URL(songItem?.source)
        val connection = url.openConnection()
        connection.connect()
        val inputStream = BufferedInputStream(url.openStream())
        val file = File(applicationContext.filesDir, filename.toString())
        val outputStream = FileOutputStream(file.path)
        val data = ByteArray(1024)
        var count = inputStream.read(data)
        var total = count
        while (count != -1) {
            outputStream.write(data, 0, count)
            count = inputStream.read(data)
            total += count
        }
        outputStream.flush()
        outputStream.close()
        inputStream.close()
    }

    private fun handleActionMusic(action: Int?) {
        when (action) {
            ACTION_PAUSE -> {
                pauseMusic()
            }
            ACTION_RESUME -> {
                resumeMusic()
            }
            ACTION_CLOSE -> {
                mediaPlayer?.release()
                mediaPlayer = null
                isStart = true
                isServiceRunning = false
                booleanFromService(isServiceRunning)
                stopForeground(true)
                stopSelf()
            }
            ACTION_NEXT -> {
                nextMusic()
            }
            ACTION_BACK -> {
                backMusic()
            }
        }
    }

    fun pauseMusic() {
        if (mediaPlayer != null && isPlaying && !isPause) {
            mediaPlayer?.pause()
            isPlaying = false
            isPause = true
            currentPosition = mediaPlayer!!.currentPosition
        }
    }

    fun resumeMusic() {
        if (mediaPlayer != null && !isPlaying && !isResume) {
            isPlaying = true
            isResume = true
            mediaPlayer!!.seekTo(currentPosition)
            mediaPlayer?.start()
        }
    }

    fun nextMusic() {
        if (!isRandom) {
            position++
            if (position+1 > arrSong!!.size) {
                position = 0
                mediaPlayer?.release()
                sendNotification(arrSong!![position])
                startMusic(arrSong!![position])
                dataSongFromService(arrSong!![position])
            } else {
                mediaPlayer?.release()
                isResume = true
                sendNotification(arrSong!![position])
                startMusic(arrSong!![position])
                dataSongFromService(arrSong!![position])
            }
        } else {
            val randomNum: Int = ThreadLocalRandom.current().nextInt(0, arrSong!!.size + 1)
            position = randomNum
            mediaPlayer?.release()
            sendNotification(arrSong!![position])
            startMusic(arrSong!![position])
            dataSongFromService(arrSong!![position])
        }
    }

    fun backMusic() {
        if (position > 0) {
            position--
            mediaPlayer?.release()
            sendNotification(arrSong!![position])
            startMusic(arrSong!![position])
            dataSongFromService(arrSong!![position])
        } else {
            position = arrSong!!.size-1
            mediaPlayer?.release()
            sendNotification(arrSong!![position])
            startMusic(arrSong!![position])
            dataSongFromService(arrSong!![position])
        }
    }

    fun seekBarChange(seekBarPosition: Int) {
        if (mediaPlayer != null) {
            mediaPlayer!!.seekTo(seekBarPosition)
            mediaPosition = seekBarPosition
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag", "RemoteViewLayout")
    private fun sendNotification(songItem: SongItem?) {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val remoteViews = RemoteViews(packageName, R.layout.custom_notification)
        try {
            val bitmap: Bitmap = Glide.with(this).asBitmap().load(songItem?.image).submit().get()
            remoteViews.setImageViewBitmap(R.id.imageSongNotification, bitmap)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        remoteViews.setTextViewText(R.id.txtNameSongNotification, songItem?.name)
        remoteViews.setTextViewText(R.id.txtNameArtistNotification, songItem?.artist)
        remoteViews.setImageViewResource(R.id.btnForward, R.drawable.ic_forward)
        remoteViews.setImageViewResource(R.id.btnRewind, R.drawable.ic_rewind)
        remoteViews.setImageViewResource(R.id.btnPlayOrPause, R.drawable.ic_pause)
        remoteViews.setImageViewResource(R.id.btnClose, R.drawable.ic_close)
        remoteViews.setTextViewText(
            R.id.txtTimeTotalNotification,
            songItem?.duration?.let { formatTime(it) })
        remoteViews.setTextViewText(
            R.id.txtTimeSongNotification,
            mediaPosition?.let { formatTime(it) })
        songItem?.duration?.let {
            mediaPosition?.let { it1 ->
                remoteViews.setProgressBar(
                    R.id.progressBarNotification,
                    it,
                    it1,
                    false
                )
            }
        }

        if (isPlaying) {
            remoteViews.setOnClickPendingIntent(
                R.id.btnPlayOrPause,
                getPendingIntent(this, ACTION_PAUSE)
            )
            remoteViews.setImageViewResource(R.id.btnPlayOrPause, R.drawable.ic_pause)
        } else {
            remoteViews.setOnClickPendingIntent(
                R.id.btnPlayOrPause,
                getPendingIntent(this, ACTION_RESUME)
            )
            remoteViews.setImageViewResource(R.id.btnPlayOrPause, R.drawable.ic_play_white)
        }
        remoteViews.setOnClickPendingIntent(R.id.btnClose, getPendingIntent(this, ACTION_CLOSE))
        remoteViews.setOnClickPendingIntent(R.id.btnForward, getPendingIntent(this, ACTION_NEXT))
        remoteViews.setOnClickPendingIntent(R.id.btnRewind, getPendingIntent(this, ACTION_BACK))

        val notification = NotificationCompat.Builder(this, Constants.CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentIntent(pendingIntent)
            .setCustomContentView(remoteViews)
            .setSound(null)
            .build()

        startForeground(1, notification)
    }

    private fun booleanFromService(isServiceRunning: Boolean?) {
        val intent = Intent(Constants.ActionIntent.BOOLEAN)
        val bundle = Bundle()
        bundle.putSerializable(Constants.Bundle.BOOLEAN, isServiceRunning)
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun dataSongFromService(songItem: SongItem?) {
        val intent = Intent(Constants.ActionIntent.SONG_UPDATE)
        val bundle = Bundle()
        bundle.putSerializable(Constants.Bundle.DATA_FROM_SERVICE, songItem)
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun dataSong(songItem: SongItem?) {
        val intent = Intent(Constants.ActionIntent.SONG)
        val bundle = Bundle()
        bundle.putSerializable(Constants.Bundle.DATA_FROM_SERVICE, songItem)
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun mediaPositionFromService(mediaPosition: Int?) {
        val intent = Intent(Constants.ActionIntent.MEDIA_POSITION)
        val bundle = Bundle()
        mediaPosition?.let { bundle.putInt(Constants.Bundle.MEDIA_POSITION_FROM_SERVICE, it) }
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun timerFromService(timer: Int?) {
        val intent = Intent(Constants.ActionIntent.TIMER)
        val bundle = Bundle()
        timer?.let { bundle.putInt(Constants.Bundle.TIMER, it) }
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun listSong(list: ArrayList<SongItem>?, pos: Int) {
        val intent = Intent(Constants.ActionIntent.LIST_SONG)
        val bundle = Bundle()
        bundle.putSerializable(Constants.Bundle.LIST_SONG, list)
        bundle.putSerializable(Constants.Bundle.POSITION, pos)
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun getPendingIntent(context: Context, action: Int): PendingIntent {
        val intent = Intent(this, Receiver::class.java)
        val bundle = Bundle()
        bundle.putInt(Constants.ACTION_MUSIC, action)
        intent.putExtras(bundle)
        return PendingIntent.getBroadcast(
            context.applicationContext,
            action,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun formatTime(pTime: Int): String {
        val min = pTime / 60
        val sec = pTime - min * 60
        val strMin: String = placeZeroIfNeed(min)
        val strSec: String = placeZeroIfNeed(sec)
        return String.format("$strMin:$strSec")
    }

    private fun placeZeroIfNeed(number: Int): String {
        return if (number >= 10) {
            number.toString()
        } else {
            String.format("0$number")
        }
    }

    override fun onUnbind(intent: Intent?): Boolean {
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mediaPlayer != null) {
            mediaPlayer?.release()
            mediaPlayer = null
        }
        handler.removeCallbacks(runnable)
    }
}