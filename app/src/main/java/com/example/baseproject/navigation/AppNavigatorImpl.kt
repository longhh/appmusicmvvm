package com.example.baseproject.navigation

import android.os.Bundle
import com.example.baseproject.R
import com.example.core.navigationComponent.BaseNavigatorImpl
import com.example.setting.DemoNavigation
import javax.inject.Inject

class AppNavigatorImpl @Inject constructor() : BaseNavigatorImpl(),
    AppNavigation, DemoNavigation {

    override fun openSplashToHomeScreen(bundle: Bundle?) {
        openScreen(R.id.action_splashFragment_to_homeFragment, bundle)
    }

    override fun openSplashToLoginScreen(bundle: Bundle?) {
        openScreen(R.id.action_homeFragment_to_loginFragment, bundle)
    }

    override fun openTabHomePageToAlbumDetail(bundle: Bundle?) {
        openScreen(R.id.action_homeFragment_to_albumDetailFragment, bundle)
    }

    override fun openTabHomePageToArtistDetail(bundle: Bundle?) {
        openScreen(R.id.action_homeFragment_to_artistDetailFragment, bundle)
    }

    override fun openTabHomePageToGenreDetail(bundle: Bundle?) {
        openScreen(R.id.action_homeFragment_to_genreDetailFragment, bundle)
    }

    override fun openTrendingToPlayScreen(bundle: Bundle?) {
        openScreen(R.id.action_homeFragment_to_playFragment, bundle)
    }

    override fun openAlbumDetailToPlayScreen(bundle: Bundle?) {
        openScreen(R.id.action_albumDetailFragment_to_playFragment, bundle)
    }

    override fun openArtistDetailToPlayScreen(bundle: Bundle?) {
        openScreen(R.id.action_artistDetailFragment_to_playFragment, bundle)
    }

    override fun openGenreDetailToPlayScreen(bundle: Bundle?) {
        openScreen(R.id.action_genreDetailFragment_to_playFragment, bundle)
    }

    override fun openPersonalMusicToPlayScreen(bundle: Bundle?) {
        openScreen(R.id.action_homeFragment_to_playFragment, bundle)
    }

    override fun openPlaylistToPlayScreen(bundle: Bundle?) {
        openScreen(R.id.action_playlistFragment_to_playFragment, bundle)
    }

    override fun openFavouriteToPlayScreen(bundle: Bundle?) {
        openScreen(R.id.action_playlistFragment_to_playFragment, bundle)
    }

    override fun openControlBarToPlayScreen(bundle: Bundle?) {
        openScreen(R.id.action_homeFragment_to_playFragment, bundle)
    }

    override fun openTabPersonalToLogin(bundle: Bundle?) {
        openScreen(R.id.action_homeFragment_to_loginFragment, bundle)
    }

    override fun openLoginToRegister(bundle: Bundle?) {
        openScreen(R.id.action_loginFragment_to_registerFragment, bundle)
    }

    override fun openLoginToHome(bundle: Bundle?) {
        openScreen(R.id.action_loginFragment_to_homeFragment, bundle)
    }

    override fun openHomeToProfileScreen(bundle: Bundle?) {
        openScreen(R.id.action_homeFragment_to_profileFragment, bundle)
    }
}