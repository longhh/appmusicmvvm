package com.example.baseproject.navigation

import android.os.Bundle
import com.example.core.navigationComponent.BaseNavigator

interface AppNavigation : BaseNavigator {

    fun openSplashToHomeScreen(bundle: Bundle? = null)

    fun openSplashToLoginScreen(bundle: Bundle? = null)

    fun openTabHomePageToAlbumDetail(bundle: Bundle? = null)

    fun openTabHomePageToArtistDetail(bundle: Bundle? = null)

    fun openTabHomePageToGenreDetail(bundle: Bundle? = null)

    fun openTrendingToPlayScreen(bundle: Bundle? = null)

    fun openAlbumDetailToPlayScreen(bundle: Bundle? = null)

    fun openArtistDetailToPlayScreen(bundle: Bundle? = null)

    fun openGenreDetailToPlayScreen(bundle: Bundle? = null)

    fun openPersonalMusicToPlayScreen(bundle: Bundle? = null)

    fun openPlaylistToPlayScreen(bundle: Bundle? = null)

    fun openFavouriteToPlayScreen(bundle: Bundle? = null)

    fun openControlBarToPlayScreen(bundle: Bundle? = null)

    fun openTabPersonalToLogin(bundle: Bundle? = null)

    fun openLoginToRegister(bundle: Bundle? = null)

    fun openLoginToHome(bundle: Bundle? = null)

    fun openHomeToProfileScreen(bundle: Bundle? = null)
}