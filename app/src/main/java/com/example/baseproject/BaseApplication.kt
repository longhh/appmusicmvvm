package com.example.baseproject

import android.annotation.SuppressLint
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.databinding.library.BuildConfig
import com.example.core.utils.Constants
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import javax.inject.Inject

@HiltAndroidApp
class BaseApplication @Inject constructor() : Application() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        createChannelNotification()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannelNotification() {
        @SuppressLint("WrongConstant") val channel = NotificationChannel(
            Constants.CHANNEL_ID, "PushNotification", NotificationManager.IMPORTANCE_DEFAULT
        )

        channel.setSound(null, null)
        getSystemService(NotificationManager::class.java).createNotificationChannel(channel)
    }

}