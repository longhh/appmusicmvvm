package com.example.baseproject.ui.tabHomePage

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.core.base.BaseViewModel
import com.example.core.model.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class HomePageViewModel @Inject constructor(private val homePageRepository: HomePageRepository) : BaseViewModel() {
    val listDataHomePage = MutableLiveData<ArrayList<HomePage>>()
    val listDataAlbum = MutableLiveData<ArrayList<Album>>()
    val listDataArtist = MutableLiveData<ArrayList<Artist>>()
    val listDataGenre = MutableLiveData<ArrayList<Genre>>()

    val progressBar = MutableLiveData<Boolean>()

    init {
        getData()
    }

    private fun getData() {
        val handlerException = CoroutineExceptionHandler { _, throwable ->
            println("CoroutineExceptionHandler got $throwable")
        }
        CoroutineScope(Dispatchers.IO + handlerException).launch {
            progressBar.postValue(true)
            try {
                val responseBanner: Deferred<ArrayList<Banner>> = async { homePageRepository.getBanner() }
                val responseAlbum: Deferred<ArrayList<Album>> = async { homePageRepository.getAlbum() }
                val responseArtist: Deferred<ArrayList<Artist>> = async { homePageRepository.getArtist() }
                val responseGenre: Deferred<ArrayList<Genre>> = async { homePageRepository.getGenre() }
                awaitAll(responseBanner, responseAlbum, responseArtist, responseGenre)

                val listHomePage = ArrayList<HomePage>()
                listHomePage.add(HomePage(0, responseBanner.await(), null, null, null))
                listHomePage.add(HomePage(1, null, responseAlbum.await(), null, null))
                listHomePage.add(HomePage(2, null, null, responseArtist.await(), null))
                listHomePage.add(HomePage(3, null, null, null, responseGenre.await()))
                listDataHomePage.postValue(listHomePage)
                listDataAlbum.postValue(responseAlbum.await())
                listDataArtist.postValue(responseArtist.await())
                listDataGenre.postValue(responseGenre.await())
                progressBar.postValue(false)
            } catch (e: Exception) {
                Log.getStackTraceString(e)
                listDataHomePage.postValue(ArrayList())
                listDataAlbum.postValue(ArrayList())
                listDataArtist.postValue(ArrayList())
                listDataGenre.postValue(ArrayList())
            }
        }
    }
}
