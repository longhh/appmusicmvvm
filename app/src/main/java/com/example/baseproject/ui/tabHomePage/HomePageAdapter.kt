package com.example.baseproject.ui.tabHomePage

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.baseproject.R
import com.example.baseproject.databinding.*
import com.example.core.model.*
import com.example.core.utils.listener.OnItemClickHomePage
import com.example.core.utils.listener.OnItemClickListener
import java.util.ArrayList
import androidx.viewpager.widget.ViewPager.OnPageChangeListener

class HomePageAdapter constructor(context: Context) :
    ListAdapter<HomePage, RecyclerView.ViewHolder>(HomePageDiffUtil()) {

    companion object {
        const val BANNER = 0
        const val ALBUM = 1
        const val ARTIST = 2
        const val GENRE = 3
    }

    private var onItemClickHomePage: OnItemClickHomePage? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickHomePage: OnItemClickHomePage?) {
        this.onItemClickHomePage = onItemClickHomePage
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            BANNER -> BannerHolder(
                ItemTypeViewpagerBinding.inflate(layoutInflater, parent, false)
            )
            ALBUM -> AlbumHolder(
                ItemTypeHorizontalBinding.inflate(layoutInflater, parent, false), onItemClickHomePage
            )
            ARTIST -> ArtistHolder(
                ItemTypeHorizontalBinding.inflate(layoutInflater, parent, false), onItemClickHomePage
            )
            GENRE -> GenreHolder(
                ItemTypeHorizontalBinding.inflate(layoutInflater, parent, false), onItemClickHomePage
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is BannerHolder -> holder.bindView(getItem(position).banner)
            is AlbumHolder -> holder.bindView(getItem(position).album)
            is ArtistHolder -> holder.bindView(getItem(position).artist)
            is GenreHolder -> holder.bindView(getItem(position).genre)
        }
    }

    override fun submitList(list: List<HomePage>?) {
        super.submitList(list?.let { ArrayList(it) })
    }


    override fun getItemViewType(position: Int): Int {
        return when (getItem(position).type) {
            0 -> {
                BANNER
            }
            1 -> {
                ALBUM
            }
            2 -> {
                ARTIST
            }
            3 -> {
                GENRE
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }
}

class BannerHolder(val binding: ItemTypeViewpagerBinding) : RecyclerView.ViewHolder(binding.root) {
    private val bannerHomePageAdapter: BannerHomePageAdapter by lazy {
        BannerHomePageAdapter(itemView.context, ArrayList())
    }

    private val handler = Handler(Looper.getMainLooper())
    private var runnable: Runnable? = null

    fun bindView(data: ArrayList<Banner>?) {
        bannerHomePageAdapter.setData(data)
        runnable = object : Runnable {
            override fun run() {
                var currentItem: Int = binding.viewPagerBanner.currentItem
                currentItem++
                if (currentItem >= binding.viewPagerBanner.adapter!!.count) {
                    currentItem = 0
                }
                binding.viewPagerBanner.setCurrentItem(currentItem, true)
                handler.postDelayed(this, 4000)
            }
        }
        binding.viewPagerBanner.adapter = bannerHomePageAdapter
        binding.circleIndicator.setViewPager(binding.viewPagerBanner)
        handler.postDelayed(runnable as Runnable, 4000)
    }
}

class AlbumHolder(var binding: ItemTypeHorizontalBinding, onItemClickHomePage: OnItemClickHomePage?) :
    RecyclerView.ViewHolder(binding.root) {
    private val albumHomePageAdapter: AlbumHomePageAdapter by lazy {
        AlbumHomePageAdapter(itemView.context)
    }

    init {
        albumHomePageAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun setOnClick(position: Int) {
                onItemClickHomePage?.setOnClick(position, 1)
            }
        })
    }

    fun bindView(data: ArrayList<Album>?) {
        albumHomePageAdapter.submitList(data)
        binding.txtTitleHomePage.text = itemView.context.getString(R.string.album)
        binding.rvHomePageHorizontal.adapter = albumHomePageAdapter
    }
}

class ArtistHolder(var binding: ItemTypeHorizontalBinding, onItemClickHomePage: OnItemClickHomePage?) :
    RecyclerView.ViewHolder(binding.root) {
    private val artistHomePageAdapter: ArtistHomePageAdapter by lazy {
        ArtistHomePageAdapter(itemView.context)
    }

    init {
        artistHomePageAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun setOnClick(position: Int) {
                onItemClickHomePage?.setOnClick(position, 2)
            }
        })
    }

    fun bindView(data: ArrayList<Artist>?) {
        artistHomePageAdapter.submitList(data)
        binding.txtTitleHomePage.text = itemView.context.getString(R.string.artist)
        binding.rvHomePageHorizontal.adapter = artistHomePageAdapter
    }
}

class GenreHolder(var binding: ItemTypeHorizontalBinding, onItemClickHomePage: OnItemClickHomePage?) :
    RecyclerView.ViewHolder(binding.root) {
    private val genreHomePageAdapter: GenreHomePageAdapter by lazy {
        GenreHomePageAdapter(itemView.context)
    }

    init {
        genreHomePageAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun setOnClick(position: Int) {
                onItemClickHomePage?.setOnClick(position, 3)
            }
        })
    }

    fun bindView(data: ArrayList<Genre>?) {
        genreHomePageAdapter.submitList(data)
        binding.txtTitleHomePage.text = itemView.context.getString(R.string.genre)
        binding.rvHomePageHorizontal.adapter = genreHomePageAdapter
    }
}

class HomePageDiffUtil : DiffUtil.ItemCallback<HomePage>() {
    override fun areItemsTheSame(oldItem: HomePage, newItem: HomePage): Boolean {
        return oldItem.banner == newItem.banner && oldItem.album == newItem.album
                && oldItem.artist == newItem.artist && oldItem.genre == newItem.genre
    }

    override fun areContentsTheSame(oldItem: HomePage, newItem: HomePage): Boolean {
        return oldItem == newItem
    }
}