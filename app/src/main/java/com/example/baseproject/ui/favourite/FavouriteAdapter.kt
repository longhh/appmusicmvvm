package com.example.baseproject.ui.favourite

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.core.model.SongItem
import java.util.*
import android.widget.PopupMenu
import com.example.baseproject.R
import com.example.baseproject.databinding.ItemFavouriteBinding
import com.example.core.utils.listener.OnItemClickPopupMenu


class FavouriteAdapter constructor(context: Context) :
    ListAdapter<SongItem, FavouriteHolder>(FavouriteDiffUtil()) {

    private var onItemClickPopupMenu: OnItemClickPopupMenu? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickPopupMenu: OnItemClickPopupMenu?) {
        this.onItemClickPopupMenu = onItemClickPopupMenu
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteHolder {
        return FavouriteHolder(
            ItemFavouriteBinding.inflate(layoutInflater, parent, false), onItemClickPopupMenu
        )
    }

    override fun onBindViewHolder(holder: FavouriteHolder, position: Int) {
        holder.bind(getItem(position), onItemClickPopupMenu)
    }

    override fun submitList(list: List<SongItem>?) {
        super.submitList(list?.let { ArrayList(it) })
    }
}

class FavouriteHolder(val binding: ItemFavouriteBinding, onItemClickListener: OnItemClickPopupMenu?) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.itemPlaylistDetail.setOnClickListener {
            onItemClickListener?.setOnClick(adapterPosition)
        }
    }

    fun bind(list: SongItem, onItemClickListener: OnItemClickPopupMenu?) {
        Glide.with(itemView.context).load(list.image).into(binding.imgCircleFavourite)
        binding.txtSongFavourite.text = list.name
        binding.txtArtistFavourite.text = list.artist

        val popupMenu = PopupMenu(itemView.context, binding.btnShowMenuFavourite)
        popupMenu.menuInflater.inflate(R.menu.menu_playlist_item, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            if (item.itemId == R.id.action_delete) {
                onItemClickListener?.setOnClickPopupMenu(adapterPosition)
            }
            true
        }

        binding.btnShowMenuFavourite.setOnClickListener {
            popupMenu.show()
        }
    }
}

class FavouriteDiffUtil : DiffUtil.ItemCallback<SongItem>() {
    override fun areItemsTheSame(oldItem: SongItem, newItem: SongItem): Boolean {
        return oldItem.id == newItem.id && oldItem.name == newItem.name
                && oldItem.source == newItem.source && oldItem.image == newItem.image
                && oldItem.artist == newItem.artist
    }

    override fun areContentsTheSame(oldItem: SongItem, newItem: SongItem): Boolean {
        return oldItem == newItem
    }

}