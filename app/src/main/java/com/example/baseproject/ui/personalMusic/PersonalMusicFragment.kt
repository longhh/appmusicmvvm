package com.example.baseproject.ui.personalMusic

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentPersonalMusicBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.utils.Constants
import com.example.core.utils.listener.OnItemClickListener
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PersonalMusicFragment :
    BaseFragment<FragmentPersonalMusicBinding, PersonalMusicViewModel>(R.layout.fragment_personal_music) {

    private val viewModel: PersonalMusicViewModel by viewModels()

    override fun getVM(): PersonalMusicViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    private val adapterPersonalMusic: PersonalMusicAdapter by lazy {
        PersonalMusicAdapter(requireContext())
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        viewModel.getPersonalMusic(requireContext())
        setupAdapter()
    }

    private fun setupAdapter() {
        binding.rvSongPersonalMusic.adapter = adapterPersonalMusic
        adapterPersonalMusic.setOnItemClickListener(object : OnItemClickListener {
            override fun setOnClick(position: Int) {
                val bundle = Bundle()
                val songItem = viewModel.listDataSongItem.value
                bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
                bundle.putInt(Constants.Bundle.POSITION, position)
                appNavigation.openPersonalMusicToPlayScreen(bundle)
            }
        })
    }

    override fun bindingStateView() {
        super.bindingStateView()
        viewModel.listDataPersonalMusic.observe(viewLifecycleOwner, {
            adapterPersonalMusic.submitList(it)
        })
    }

}