package com.example.baseproject.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.core.base.BaseViewModel
import com.example.core.model.Users
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(private val savedStateHandle: SavedStateHandle) : BaseViewModel() {
    val resultUser = MutableLiveData<Users?>()

    init {
        getUser()
    }

    private fun getUser() {
        val users: Users? = savedStateHandle.get(Constants.Bundle.USERS)
        if (users != null) {
            resultUser.postValue(users)
        }
    }

    fun signOut(){
        FirebaseAuth.getInstance().signOut()
    }
}
