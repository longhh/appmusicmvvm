package com.example.baseproject.ui.genreDetail

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.core.base.BaseViewModel
import com.example.core.model.Genre
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GenreDetailViewModel @Inject constructor(
    private val genreDetailRepository: GenreDetailRepository,
    private val savedStateHandle: SavedStateHandle
) :
    BaseViewModel() {
    val listSongGenre = MutableLiveData<ArrayList<SongItem>>()
    val imageGenreLive = MutableLiveData<String>()
    val nameGenreLive = MutableLiveData<String>()
    val progressBar = MutableLiveData<Boolean>()

    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private val arrayListSongGenre: ArrayList<SongItem> = ArrayList()
    private var nameArtist: String? = null

    init {
        getSongGenre()
    }

    private fun getSongGenre() {

        CoroutineScope(Dispatchers.IO).launch {
            progressBar.postValue(true)
            try {
                val responseSong = genreDetailRepository.getSong()
                val responseArtist = genreDetailRepository.getArtist()
                val genre: Genre = savedStateHandle.get(Constants.Bundle.GENRE)!!

                for (i in responseSong.indices) {
                    if (genre.id != null) {
                        if (genre.id == responseSong[i].genreId) {
                            val nameSong: String? = responseSong[i].title
                            val imageSong: String?  = responseSong[i].image
                            val sourceSong: String? = responseSong[i].source
                            val artistId: Int? = responseSong[i].artistId
                            val durationSong: Int? = responseSong[i].duration
                            for (j in responseArtist.indices) {
                                if (artistId == responseArtist[j].id) {
                                    nameArtist = responseArtist[j].name
                                }
                            }
                            arrayListSongGenre.add(SongItem(i, nameSong, imageSong, sourceSong, nameArtist, durationSong))
                        }
                    }
                }

                listSongGenre.postValue(arrayListSongGenre)
                imageGenreLive.postValue(genre.image!!)
                nameGenreLive.postValue(genre.name!!)
                progressBar.postValue(false)
            } catch (e: Exception) {
                Log.getStackTraceString(e)
                listSongGenre.postValue(ArrayList())
            }
        }
    }
    fun addFavourite(songItem: SongItem) {
        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Firebase.REFERENCE_FAVOURITE)
                .child(firebaseUser.uid)
                .push()
                .setValue(songItem)
        }
    }
}
