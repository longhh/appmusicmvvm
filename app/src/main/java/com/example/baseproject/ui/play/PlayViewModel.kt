package com.example.baseproject.ui.play

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.core.base.BaseViewModel
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PlayViewModel @Inject constructor(private val savedStateHandle: SavedStateHandle) :
    BaseViewModel() {
    var result = false
    val dataSong = MutableLiveData<ArrayList<SongItem>>()
    val songItem = MutableLiveData<ArrayList<SongItem>?>()
    val position = MutableLiveData<Int>()
    val resultFavourite = MutableLiveData(false)

    private var favourite: ArrayList<SongItem> = ArrayList()
    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private var songItemFromControlBar: ArrayList<SongItem>? = null
    private var listSong: ArrayList<SongItem>? = null
    private var positionFromArg: Int? = null

    init {
        getDataSong()
        if (listSong!=null) {
            getDataFavourite(listSong!![positionFromArg!!].name)
        } else {
            getDataFavourite(songItemFromControlBar!![positionFromArg!!].name)
        }
    }

    private fun getDataSong() {
        positionFromArg = savedStateHandle.get(Constants.Bundle.POSITION)
        listSong = savedStateHandle.get(Constants.Bundle.SONG_ITEM)
        songItemFromControlBar = savedStateHandle.get(Constants.Bundle.SONG_ITEM_FROM_CONTROL_BAR)
        try {
            position.postValue(positionFromArg!!)
            if (songItemFromControlBar != null) {
                songItem.postValue(songItemFromControlBar)
            } else {
                dataSong.postValue(listSong!!)
            }
        } catch (e: Exception) {
            Log.getStackTraceString(e)
        }
    }

    fun getDataFavourite(nameSong: String?) {
        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Firebase.REFERENCE_FAVOURITE)
                .child(firebaseUser.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        favourite = ArrayList()
                        for (snapshot in dataSnapshot.children) {
                            val song = snapshot.getValue(SongItem::class.java)
                            if (song != null) {
                                if (song.name.toString() == nameSong) {
                                    result = true
                                }
                            }
                        }
                        if (result) {
                            resultFavourite.postValue(true)
                            result = false
                        } else {
                            resultFavourite.postValue(false)
                        }
                    }
                    override fun onCancelled(databaseError: DatabaseError) {
                    }
                })
        }
    }

    fun removeFavourite(nameSong: String) {
        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Firebase.REFERENCE_FAVOURITE)
                .child(firebaseUser.uid)
                .addListenerForSingleValueEvent(object : ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {
                        for (i in snapshot.children) {
                            val song = i.getValue(SongItem::class.java)
                            if (song!!.name == nameSong) {
                                i.ref.removeValue()
                            }
                        }
                    }
                    override fun onCancelled(error: DatabaseError) {

                    }

                })
        }
    }

    fun addFavourite(songItem: SongItem) {
        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Firebase.REFERENCE_FAVOURITE)
                .child(firebaseUser.uid)
                .push()
                .setValue(songItem)
        }
    }
}
