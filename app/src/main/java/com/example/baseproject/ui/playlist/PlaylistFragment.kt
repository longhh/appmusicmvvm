package com.example.baseproject.ui.playlist

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.baseproject.R
import com.example.baseproject.databinding.CustomDialogAddPlaylistBinding
import com.example.baseproject.databinding.FragmentPlaylistBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.database.SQLitePlaylist
import com.example.core.utils.listener.OnItemClickPlaylist
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PlaylistFragment :
    BaseFragment<FragmentPlaylistBinding, PlaylistViewModel>(R.layout.fragment_playlist) {

    private val viewModel: PlaylistViewModel by viewModels()

    override fun getVM(): PlaylistViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var sqLitePlaylist: SQLitePlaylist

    private val playlistAdapter: PlaylistAdapter by lazy {
        PlaylistAdapter(requireContext())
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupAdapter()
    }

    private fun setupAdapter() {
        binding.rvPlaylist.adapter = playlistAdapter
        playlistAdapter.submitList(sqLitePlaylist.getDataPlaylist())
        binding.btnAddPlaylist.setOnClickListener {
            dialogCreatePlaylist()
        }
        playlistAdapter.setOnItemClickListener(object : OnItemClickPlaylist {
            override fun deleteClick(position: Int) {
                dialogDeletePlaylist(position)
            }

            override fun itemClick(position: Int) {
//                appNavigation.openPlaylistToPlaylistDetails()
            }
        })
    }

    private fun dialogCreatePlaylist() {
        val dialog = Dialog(requireContext(), R.style.DialogTheme)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        val binding : CustomDialogAddPlaylistBinding = CustomDialogAddPlaylistBinding.inflate(LayoutInflater.from(context))
        dialog.setContentView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding.btnYes.setOnClickListener {
            val name: String = binding.edtPlaylist.text.toString()
            sqLitePlaylist.insertDataPlaylist(name)
            playlistAdapter.submitList(sqLitePlaylist.getDataPlaylist())
            dialog.dismiss()
        }
        binding.btnCancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    private fun dialogDeletePlaylist(position: Int) {
        val alertDialog = context?.let { AlertDialog.Builder(it) }
        val name: String? = sqLitePlaylist.getDataPlaylist()[position].name
        val id: Int? = sqLitePlaylist.getDataPlaylist()[position].id
        alertDialog?.setMessage(getString(R.string.delete_playlist))
        alertDialog?.setPositiveButton(getString(R.string.yes)) { _, _ ->
            sqLitePlaylist.deleteDataPlaylist(id)
            Toast.makeText(context, getString(R.string.deleted_playlist) + name , Toast.LENGTH_SHORT).show()
            playlistAdapter.submitList(sqLitePlaylist.getDataPlaylist())
        }
        alertDialog?.setNegativeButton(getString(R.string.no)) { _, _ -> }
        alertDialog?.show()
    }
}