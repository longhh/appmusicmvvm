package com.example.baseproject.ui.artistDetail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentArtistDetailBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.database.SQLitePlaylistDetail
import com.example.core.utils.Constants
import com.example.core.utils.listener.OnItemClickPopupAddSong
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ArtistDetailFragment :
    BaseFragment<FragmentArtistDetailBinding, ArtistDetailViewModel>(R.layout.fragment_artist_detail) {

    private val viewModel: ArtistDetailViewModel by viewModels()

    override fun getVM(): ArtistDetailViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var sqLitePlaylistDetail: SQLitePlaylistDetail

    private val adapterArtistDetail: ArtistDetailAdapter by lazy {
        ArtistDetailAdapter(requireContext())
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupAdapter()
        setupView()
    }

    private fun setupAdapter() {
        binding.rvSongArtist.adapter = adapterArtistDetail
    }

    private fun setupView() {
        binding.btnArtistBackHomepage.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    override fun bindingStateView() {
        super.bindingStateView()
        viewModel.progressBar.observe(viewLifecycleOwner, {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })
        viewModel.listSongArtist.observe(viewLifecycleOwner, {
            adapterArtistDetail.submitList(it)
            adapterArtistDetail.setOnItemClickListener(object : OnItemClickPopupAddSong {
                override fun setOnClick(position: Int) {
                    val bundle = Bundle()
                    val songItem = viewModel.listSongArtist.value
                    bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
                    bundle.putInt(Constants.Bundle.POSITION, position)
                    appNavigation.openArtistDetailToPlayScreen(bundle)
                }

                override fun setOnClickAddPlaylist(position: Int) {
                    sqLitePlaylistDetail.insertDataPlaylistDetail(
                        it[position].name,
                        it[position].image,
                        it[position].source,
                        it[position].artist,
                        it[position].duration
                    )
                    Toast.makeText(context, getString(R.string.added_to_playlist), Toast.LENGTH_SHORT).show()
                }

                override fun setOnClickAddFavourite(position: Int) {
                    viewModel.addFavourite(it[position])
                    Toast.makeText(context, getString(R.string.added_to_favourite), Toast.LENGTH_SHORT).show()
                }
            })
            binding.btnPlayArtist.setOnClickListener {
                val bundle = Bundle()
                val songItem = viewModel.listSongArtist.value
                bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
                bundle.putInt(Constants.Bundle.POSITION, 0)
                appNavigation.openArtistDetailToPlayScreen(bundle)
            }
        })

        viewModel.imageArtistLive.observe(viewLifecycleOwner, {
            Glide.with(requireContext()).load(it)
                .transform(CenterCrop(), RoundedCorners(20))
                .into(binding.imgArtistDetail)
        })

        viewModel.nameArtistLive.observe(viewLifecycleOwner, {
            binding.txtTitleArtistDetail.text = it
        })
    }
}