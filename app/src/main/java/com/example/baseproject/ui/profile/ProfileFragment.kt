package com.example.baseproject.ui.profile

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.res.Configuration
import androidx.core.app.ActivityCompat.recreate
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentProfileBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class ProfileFragment :
    BaseFragment<FragmentProfileBinding, ProfileViewModel>(R.layout.fragment_profile) {

    private val viewModel: ProfileViewModel by viewModels()

    override fun getVM(): ProfileViewModel = viewModel

    private var listLanguage: ArrayList<String> = ArrayList()
    private var listItem= arrayOf("Tiếng Việt", "English")

    @Inject
    lateinit var appNavigation: AppNavigation

    override fun setOnClick() {
        super.setOnClick()
        loadLocate()
        binding.languageCurrent.setOnClickListener {
            showChangeLanguageDialog()
        }
        binding.changeLanguage.setOnClickListener {
            showChangeLanguageDialog()
        }
        binding.imgSignOut.setOnClickListener {
            alertDialogSignOut()
        }
        binding.txtSignOut.setOnClickListener {
            alertDialogSignOut()
        }
        binding.btnProfileBackHomepage.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    override fun bindingStateView() {
        super.bindingStateView()
        viewModel.resultUser.observe(viewLifecycleOwner, {
            if (it != null) {
                if (it.imageURL.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                    Glide.with(requireContext()).load(R.drawable.ic_launcher_foreground).into(binding.imgCircleProfile)
                    Glide.with(requireContext()).load(R.drawable.ic_launcher_foreground).centerCrop().into(binding.imgProfile)
                } else {
                    Glide.with(requireContext()).load(it.imageURL).into(binding.imgCircleProfile)
                    Glide.with(requireContext()).load(it.imageURL).centerCrop().into(binding.imgProfile)
                }
                binding.txtNameProfile.text = it.username
            }
        })
    }

    private fun alertDialogSignOut() {
        androidx.appcompat.app.AlertDialog.Builder(requireContext())
            .setTitle(R.string.sign_out)
            .setMessage(R.string.mess_sign_out)
            .setPositiveButton(
                android.R.string.yes
            ) { _, _ -> signOut() }
            .setNegativeButton(android.R.string.no, null)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    private fun signOut() {
        viewModel.signOut()
        val intent = requireActivity().intent
        requireActivity().finish()
        startActivity(intent)
    }

    private fun showChangeLanguageDialog() {
        listLanguage.add("vi")
        listLanguage.add("en")
        val builder = AlertDialog.Builder(context)
            .setTitle(R.string.choose_language)
            .setSingleChoiceItems(listItem, getIndexLanguage(Locale.getDefault().language)) { dialog, which ->
                if (which == 0) {
                    setLocate("vi")
                    recreate(requireActivity())
                } else if (which == 1) {
                    setLocate("en")
                    recreate(requireActivity())
                }
                dialog.dismiss()
            }
        val alertDialog = builder.create()
        alertDialog.show()
    }

    private fun getIndexLanguage(locale: String): Int {
        for (i in listLanguage.indices) {
            if (listLanguage[i] == locale) {
                return listLanguage.indexOf(locale)
            }
        }
        return -1
    }

    private fun setLocate(lang: String) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        context?.resources?.updateConfiguration(config, requireContext().resources.displayMetrics)

        val editor = activity?.getSharedPreferences("Settings", Context.MODE_PRIVATE)?.edit()
        editor?.putString("MyLang", lang)
        editor?.apply()
    }

    private fun loadLocate() {
        val sharedPreferences = activity?.getSharedPreferences("Settings", Activity.MODE_PRIVATE)
        val language = sharedPreferences?.getString("MyLang", "")
        language?.let { setLocate(it) }
    }
}