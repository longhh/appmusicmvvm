package com.example.baseproject.ui.favourite

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentFavouriteBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.database.SQLiteFavourite
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import com.example.core.utils.listener.OnItemClickPopupMenu
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FavouriteFragment :
    BaseFragment<FragmentFavouriteBinding, FavouriteViewModel>(R.layout.fragment_favourite) {

    private val viewModel: FavouriteViewModel by viewModels()

    override fun getVM(): FavouriteViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var sqLiteFavourite: SQLiteFavourite

    private val adapterFavourite: FavouriteAdapter by lazy {
        FavouriteAdapter(requireContext())
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupAdapter()
    }

    private fun setupAdapter() {
        binding.rvSongFavourite.adapter = adapterFavourite
        viewModel.arrFavourite.observe(viewLifecycleOwner, {
            adapterFavourite.submitList(it)
            adapterFavourite.setOnItemClickListener(object : OnItemClickPopupMenu {
                override fun setOnClick(position: Int) {
                    val bundle = Bundle()
                    val songItem = it
                    bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
                    bundle.putInt(Constants.Bundle.POSITION, position)
                    appNavigation.openFavouriteToPlayScreen(bundle)
                }

                override fun setOnClickPopupMenu(position: Int) {
                    dialogDeleteFavourite(position, it)
                }
            })
        })

        binding.btnPlayFavourite.setOnClickListener {
            val bundle = Bundle()
            val songItem = sqLiteFavourite.getDataFavourite()
            bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
            bundle.putInt(Constants.Bundle.POSITION, 0)
            appNavigation.openFavouriteToPlayScreen(bundle)
        }
    }

    private fun dialogDeleteFavourite(position: Int, arr: ArrayList<SongItem>) {
        val alertDialog = context?.let { AlertDialog.Builder(it) }
        alertDialog?.setMessage(getString(R.string.delete_from_favourite))
        alertDialog?.setPositiveButton(getString(R.string.yes)) { _, _ ->
            viewModel.removeFavourite(arr[position].name.toString())
            Toast.makeText(context, getString(R.string.deleted_from_favourite), Toast.LENGTH_SHORT).show()
        }
        alertDialog?.setNegativeButton(getString(R.string.no)) { _, _ -> }
        alertDialog?.show()
    }
}