package com.example.baseproject.ui.playlistDetail

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentPlaylistDetailBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.database.SQLitePlaylistDetail
import com.example.core.utils.Constants
import com.example.core.utils.listener.OnItemClickPopupMenu
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PlaylistDetailFragment :
    BaseFragment<FragmentPlaylistDetailBinding, PlaylistDetailViewModel>(R.layout.fragment_playlist_detail) {

    private val viewModel: PlaylistDetailViewModel by viewModels()

    override fun getVM(): PlaylistDetailViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var sqLitePlaylistDetail: SQLitePlaylistDetail

    private val adapterPlaylistDetail: PlaylistDetailAdapter by lazy {
        PlaylistDetailAdapter(requireContext())
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupAdapter()
        setupView()
    }

    private fun setupAdapter() {
        binding.rvSongPlaylist.adapter = adapterPlaylistDetail
        adapterPlaylistDetail.submitList(sqLitePlaylistDetail.getDataPlaylistDetail())
        adapterPlaylistDetail.setOnItemClickListener(object : OnItemClickPopupMenu {
            override fun setOnClick(position: Int) {
                val bundle = Bundle()
                val songItem = sqLitePlaylistDetail.getDataPlaylistDetail()
                bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
                bundle.putInt(Constants.Bundle.POSITION, position)
                appNavigation.openPlaylistToPlayScreen(bundle)
            }

            override fun setOnClickPopupMenu(position: Int) {
                dialogDeletePlaylist(position)
            }
        })
        binding.btnPlayPlaylist.setOnClickListener {
            val bundle = Bundle()
            val songItem = sqLitePlaylistDetail.getDataPlaylistDetail()
            bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
            bundle.putInt(Constants.Bundle.POSITION, 0)
            appNavigation.openPlaylistToPlayScreen(bundle)
        }
    }

    private fun setupView() {

    }

    private fun dialogDeletePlaylist(position: Int) {
        val alertDialog = context?.let { AlertDialog.Builder(it) }
        val id: Int? = sqLitePlaylistDetail.getDataPlaylistDetail()[position].id
        alertDialog?.setMessage(getString(R.string.delete_from_playlist))
        alertDialog?.setPositiveButton(getString(R.string.yes)) { _, _ ->
            sqLitePlaylistDetail.deleteDataPlaylist(id)
            Toast.makeText(context, getString(R.string.deleted_from_playlist), Toast.LENGTH_SHORT).show()
            adapterPlaylistDetail.submitList(sqLitePlaylistDetail.getDataPlaylistDetail())
        }
        alertDialog?.setNegativeButton(getString(R.string.no)) { _, _ -> }
        alertDialog?.show()
    }
}