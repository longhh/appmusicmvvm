package com.example.baseproject.ui.personalMusic

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.baseproject.databinding.ItemPersonalMusicBinding
import com.example.core.model.SongPersonal
import com.example.core.utils.StringUtils.isIntegerNumber
import com.example.core.utils.listener.OnItemClickListener
import java.util.*

class PersonalMusicAdapter constructor(context: Context) :
    ListAdapter<SongPersonal, PersonalMusicHolder>(PersonalMusicDiffUtil()) {

    private var onItemClickListener: OnItemClickListener? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        this.onItemClickListener = onItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonalMusicHolder {
        return PersonalMusicHolder(
            ItemPersonalMusicBinding.inflate(layoutInflater, parent, false), onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: PersonalMusicHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun submitList(list: List<SongPersonal>?) {
        super.submitList(list?.let { ArrayList(it) })
    }
}

class PersonalMusicHolder(val binding: ItemPersonalMusicBinding, onItemClickListener: OnItemClickListener?) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.itemPersonalMusic.setOnClickListener {
            onItemClickListener?.setOnClick(adapterPosition)
        }
    }

    fun bind(list: SongPersonal) {
        if (list.image!!.isIntegerNumber) {
            Glide.with(itemView.context).load(list.image!!.toInt()).into(binding.imgCirclePersonalMusic)
        } else {
            Glide.with(itemView.context).load(Uri.parse(list.image)).into(binding.imgCirclePersonalMusic)
        }
        binding.txtSongPersonalMusic.text = list.name
        binding.txtArtistPersonalMusic.text = list.artist
    }
}

class PersonalMusicDiffUtil : DiffUtil.ItemCallback<SongPersonal>() {
    override fun areItemsTheSame(oldItem: SongPersonal, newItem: SongPersonal): Boolean {
        return oldItem.id == newItem.id && oldItem.name == newItem.name
                && oldItem.source == newItem.source && oldItem.image == newItem.image
                && oldItem.artist == newItem.artist
    }

    override fun areContentsTheSame(oldItem: SongPersonal, newItem: SongPersonal): Boolean {
        return oldItem == newItem
    }

}