package com.example.baseproject.ui.tabTrending

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentTrendingBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.database.SQLitePlaylistDetail
import com.example.core.utils.Constants
import com.example.core.utils.listener.OnItemClickPopupAddSong
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TrendingFragment :
    BaseFragment<FragmentTrendingBinding, TrendingViewModel>(R.layout.fragment_trending) {

    private val viewModel: TrendingViewModel by viewModels()

    override fun getVM(): TrendingViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var sqLitePlaylistDetail: SQLitePlaylistDetail

    private val adapterTrending: TrendingAdapter by lazy {
        TrendingAdapter(requireContext())
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupAdapter()
    }

    private fun setupAdapter() {
        Glide.with(requireContext()).load(R.drawable.logo_trending).transform(CenterCrop(), RoundedCorners(20))
            .into(binding.imgTrending)
        binding.rvSongTrending.adapter = adapterTrending
    }


    override fun bindingStateView() {
        super.bindingStateView()
        viewModel.progressBar.observe(viewLifecycleOwner, {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })
        viewModel.listSongTrending.observe(viewLifecycleOwner, {
            adapterTrending.submitList(it)
            adapterTrending.setOnItemClickListener(object : OnItemClickPopupAddSong {
                override fun setOnClick(position: Int) {
                    val bundle = Bundle()
                    val songItem = viewModel.listSongTrending.value
                    bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
                    bundle.putInt(Constants.Bundle.POSITION, position)
                    appNavigation.openTrendingToPlayScreen(bundle)
                }

                override fun setOnClickAddPlaylist(position: Int) {
                    sqLitePlaylistDetail.insertDataPlaylistDetail(
                        it[position].name,
                        it[position].image,
                        it[position].source,
                        it[position].artist,
                        it[position].duration
                    )
                    Toast.makeText(context, getString(R.string.added_to_playlist), Toast.LENGTH_SHORT).show()
                }

                override fun setOnClickAddFavourite(position: Int) {
                    viewModel.addFavourite(it[position])
                    Toast.makeText(context, getString(R.string.added_to_favourite), Toast.LENGTH_SHORT).show()
                }
            })
            binding.btnPlayTrending.setOnClickListener {
                val bundle = Bundle()
                val songItem = viewModel.listSongTrending.value
                bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
                bundle.putInt(Constants.Bundle.POSITION, 0)
                appNavigation.openTrendingToPlayScreen(bundle)
            }
        })
    }
}