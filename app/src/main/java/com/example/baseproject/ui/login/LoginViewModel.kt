package com.example.baseproject.ui.login

import androidx.lifecycle.MutableLiveData
import com.example.core.base.BaseViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor() : BaseViewModel() {
    val resultData = MutableLiveData(0)

    fun login(email: String, pass: String){
        val auth: FirebaseAuth = Firebase.auth
        auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful && email.isNotEmpty() && pass.isNotEmpty()) {
                    resultData.postValue(1)
                } else {
                    resultData.postValue(2)
                }
            }
    }
}
