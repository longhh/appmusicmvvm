package com.example.baseproject.ui.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentLoginBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>(R.layout.fragment_login) {

    @Inject
    lateinit var appNavigation: AppNavigation

    private val viewModel: LoginViewModel by viewModels()
    override fun getVM() = viewModel

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupListener()
        changeColorButton()
    }

    private fun setupListener(){
        binding.btnLogin.setOnClickListener{
            val email : String = binding.edtLoginEmail.text.trim().toString()
            val pass : String = binding.edtLoginPassword.text.trim().toString()
            viewModel.login(email, pass)
        }

        binding.txtRegister.setOnClickListener{
            appNavigation.openLoginToRegister()
        }

        binding.imgBackLogin.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    override fun bindingStateView() {
        super.bindingStateView()
        viewModel.resultData.observe(viewLifecycleOwner, {
            it?.let {
                if (it == 1) {
                    appNavigation.openLoginToHome()
                } else if (it == 2) {
                    Toast.makeText(activity, getString(R.string.login_field), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun changeColorButton() {
        binding.edtLoginEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtLoginEmail.text!!.isNotEmpty() && binding.edtLoginPassword.text!!.isNotEmpty()) {
                    binding.btnLogin.setBackgroundResource(R.drawable.custom_button)
                } else {
                    binding.btnLogin.setBackgroundResource(R.drawable.custom_button_gray)
                }
            }
        })
        binding.edtLoginPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtLoginEmail.text!!.isNotEmpty() && binding.edtLoginPassword.text!!.isNotEmpty()) {
                    binding.btnLogin.setBackgroundResource(R.drawable.custom_button)
                } else {
                    binding.btnLogin.setBackgroundResource(R.drawable.custom_button_gray)
                }
            }
        })
    }
}