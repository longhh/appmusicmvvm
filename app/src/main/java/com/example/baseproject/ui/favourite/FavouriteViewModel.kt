package com.example.baseproject.ui.favourite

import androidx.lifecycle.MutableLiveData
import com.example.core.base.BaseViewModel
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FavouriteViewModel @Inject constructor() : BaseViewModel() {

    var arrFavourite = MutableLiveData<ArrayList<SongItem>>()

    private var favourite: ArrayList<SongItem> = ArrayList()
    private val firebaseUser = FirebaseAuth.getInstance().currentUser

    init {
        getDataFavourite()
    }

    private fun getDataFavourite() {
        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Firebase.REFERENCE_FAVOURITE)
                .child(firebaseUser.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        favourite = ArrayList()
                        for (snapshot in dataSnapshot.children) {
                            val dataSong = snapshot.getValue(SongItem::class.java)
                            if (dataSong != null) {
                                favourite.add(dataSong)
                                arrFavourite.postValue(favourite)
                            }
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                    }
                })
        }
    }

    fun removeFavourite(name: String) {
        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Firebase.REFERENCE_FAVOURITE)
                .child(firebaseUser.uid)
                .addValueEventListener(object : ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {
                        for (dataSnapshot in snapshot.children) {
                            val fav = dataSnapshot.getValue(SongItem::class.java)
                            if (fav?.name.equals(name)) {
                                dataSnapshot.ref.removeValue()
                            }
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }

                })
        }
        getDataFavourite()
    }
}
