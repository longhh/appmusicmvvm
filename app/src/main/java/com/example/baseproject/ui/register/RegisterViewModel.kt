package com.example.baseproject.ui.register

import androidx.lifecycle.MutableLiveData
import com.example.core.base.BaseViewModel
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor() : BaseViewModel() {
    val resultData = MutableLiveData(0)
    fun register(username: String, email: String, pass: String) {
        val auth: FirebaseAuth = Firebase.auth
        auth.createUserWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    assert(auth.currentUser != null)
                    val user = Firebase.auth.currentUser
                    user?.let {
                        val userid = user.uid
                        val hashMap = HashMap<String, String>()
                        hashMap[Constants.Firebase.FIRE_BASE_ID] = userid
                        hashMap[Constants.Firebase.FIRE_BASE_USERNAME] = username
                        hashMap[Constants.Firebase.FIRE_BASE_IMAGE_URL] = Constants.Firebase.FIRE_BASE_DEFAULT

                        FirebaseDatabase.getInstance()
                            .getReference(Constants.Firebase.FIRE_BASE_USERS)
                            .child(userid)
                            .setValue(hashMap)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    login(email, pass)
                                } else {
                                    resultData.postValue(2)
                                }
                            }
                    }
                }
            }
    }

    private fun login(email: String, pass: String){
        val auth: FirebaseAuth = Firebase.auth
        auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    resultData.postValue(1)
                } else {
                    resultData.postValue(2)
                }
            }
    }
}
