package com.example.baseproject.ui.artistDetail

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.core.base.BaseViewModel
import com.example.core.model.Artist
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArtistDetailViewModel @Inject constructor(
    private val artistDetailRepository: ArtistDetailRepository,
    private val savedStateHandle: SavedStateHandle
) :
    BaseViewModel() {
    val listSongArtist = MutableLiveData<ArrayList<SongItem>>()
    val imageArtistLive = MutableLiveData<String>()
    val nameArtistLive = MutableLiveData<String>()
    val progressBar = MutableLiveData<Boolean>()

    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private val arrayListSongItem: ArrayList<SongItem> = ArrayList()

    init {
        getSongArtist()
    }

    private fun getSongArtist() {
        CoroutineScope(Dispatchers.IO).launch {
            progressBar.postValue(true)
            try {
                val responseSong = artistDetailRepository.getSong()
                val artist: Artist = savedStateHandle.get(Constants.Bundle.ARTIST)!!

                for (i in responseSong.indices) {
                    if (artist.id != null) {
                        if (artist.id == responseSong[i].artistId) {
                            val nameSong: String? = responseSong[i].title
                            val imageSong: String? = responseSong[i].image
                            val sourceSong: String? = responseSong[i].source
                            val durationSong: Int? = responseSong[i].duration
                            arrayListSongItem.add(SongItem(i, nameSong, imageSong, sourceSong, artist.name, durationSong))
                        }
                    }

                }

                listSongArtist.postValue(arrayListSongItem)
                imageArtistLive.postValue(artist.image!!)
                nameArtistLive.postValue(artist.name!!)
                progressBar.postValue(false)
            } catch (e: Exception) {
                Log.getStackTraceString(e)
                listSongArtist.postValue(ArrayList())
            }
        }
    }

    fun addFavourite(songItem: SongItem) {
        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Firebase.REFERENCE_FAVOURITE)
                .child(firebaseUser.uid)
                .push()
                .setValue(songItem)
        }
    }
}
