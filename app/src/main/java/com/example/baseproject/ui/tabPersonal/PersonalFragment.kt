package com.example.baseproject.ui.tabPersonal

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentPersonalBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.baseproject.ui.favourite.FavouriteFragment
import com.example.baseproject.ui.personalMusic.PersonalMusicFragment
import com.example.baseproject.ui.playlistDetail.PlaylistDetailFragment
import com.example.core.base.BaseFragment
import com.example.core.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PersonalFragment :
    BaseFragment<FragmentPersonalBinding, PersonalViewModel>(R.layout.fragment_personal) {

    private val viewModel: PersonalViewModel by viewModels()

    override fun getVM(): PersonalViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        checkAndRequestPermissions()
    }

    private val requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
        val adapter = PersonalViewPagerAdapter(childFragmentManager)
        if (isGranted) {
            adapter.addFragment(PersonalMusicFragment(), requireContext().getString(R.string.personal_music))
            adapter.addFragment(PlaylistDetailFragment(), requireContext().getString(R.string.playlist))
            adapter.addFragment(FavouriteFragment(), requireContext().getString(R.string.favourite))
            binding.viewPagePersonal.adapter = adapter
            binding.tabLayoutPersonal.setupWithViewPager(binding.viewPagePersonal)
        } else {
            Toast.makeText(requireContext(), getString(R.string.denied_permission), Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkAndRequestPermissions() {
        val readPermission = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
        val listPermissionsNeeded: MutableList<String> = ArrayList()
        if (readPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(requireActivity(), listPermissionsNeeded.toTypedArray(), 1)
        }
        requestPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
    }

    override fun bindingStateView() {
        super.bindingStateView()
        viewModel.progressBar.observe(viewLifecycleOwner, {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })
        viewModel.checkUser.observe(viewLifecycleOwner, {
            if(it == 1){
                Glide.with(requireContext()).load(R.drawable.ic_launcher_foreground).into(binding.imgCircleUser)
                Glide.with(requireContext()).load(R.drawable.ic_launcher_foreground).centerCrop().into(binding.imgUser)
                binding.txtLogin.text = getString(R.string.login)
                binding.txtLogin.setOnClickListener {
                    appNavigation.openTabPersonalToLogin()
                }
            } else {
                viewModel.resultUser.observe(viewLifecycleOwner, { it1 ->
                    if (it1 != null) {
                        if (it1.imageURL.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                            Glide.with(requireContext()).load(R.drawable.ic_launcher_foreground).into(binding.imgCircleUser)
                            Glide.with(requireContext()).load(R.drawable.ic_launcher_foreground).centerCrop().into(binding.imgUser)
                        } else {
                            Glide.with(requireContext()).load(it1.imageURL).into(binding.imgCircleUser)
                            Glide.with(requireContext()).load(it1.imageURL).centerCrop().into(binding.imgUser)
                        }
                        binding.txtLogin.text = it1.username
                        binding.txtLogin.setOnClickListener {
                            val bundle = Bundle()
                            bundle.putSerializable(Constants.Bundle.USERS, it1)
                            appNavigation.openHomeToProfileScreen(bundle)
                        }
                        binding.imgCircleUser.setOnClickListener {
                            val bundle = Bundle()
                            bundle.putSerializable(Constants.Bundle.USERS, it1)
                            appNavigation.openHomeToProfileScreen(bundle)
                        }
                    }
                })
            }
        })
    }
}