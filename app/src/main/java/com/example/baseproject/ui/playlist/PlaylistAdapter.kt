package com.example.baseproject.ui.playlist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.example.baseproject.R
import com.example.baseproject.databinding.ItemPlaylistBinding
import com.example.core.model.Playlist
import com.example.core.utils.listener.OnItemClickPlaylist
import java.util.*

class PlaylistAdapter constructor(context: Context) :
    ListAdapter<Playlist, PlaylistHolder>(PlaylistDiffUtil()) {

    private var onItemClickPlaylist: OnItemClickPlaylist? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickPlaylist: OnItemClickPlaylist?) {
        this.onItemClickPlaylist = onItemClickPlaylist
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistHolder {
        return PlaylistHolder(ItemPlaylistBinding.inflate(layoutInflater, parent, false), onItemClickPlaylist)
    }

    override fun onBindViewHolder(holder: PlaylistHolder, position: Int) {
        holder.bind(getItem(position), onItemClickPlaylist)
    }

    override fun submitList(list: List<Playlist>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

}

class PlaylistHolder(val binding: ItemPlaylistBinding, onItemClickPlaylist: OnItemClickPlaylist?) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.layoutPlaylist.setOnClickListener {
            onItemClickPlaylist?.itemClick(adapterPosition)
        }
    }

    fun bind(list: Playlist, onItemClickPlaylist: OnItemClickPlaylist?) {
        binding.txtTitlePlaylist.text = list.name
        binding.swipeLayout.showMode = SwipeLayout.ShowMode.PullOut
        binding.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, binding.swipeLayout.findViewById(R.id.layoutDisagree))
        binding.swipeLayout.findViewById<View>(R.id.txtDisagree).setOnClickListener {
            onItemClickPlaylist?.deleteClick(adapterPosition)
        }
    }
}

class PlaylistDiffUtil : DiffUtil.ItemCallback<Playlist>() {
    override fun areItemsTheSame(oldItem: Playlist, newItem: Playlist): Boolean {
        return oldItem.id == newItem.id && oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: Playlist, newItem: Playlist): Boolean {
        return oldItem == newItem
    }
}