package com.example.baseproject.ui.tabPersonal

import androidx.lifecycle.MutableLiveData
import com.example.core.base.BaseViewModel
import com.example.core.model.Users
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PersonalViewModel @Inject constructor() : BaseViewModel() {
    val checkUser = MutableLiveData<Int>()
    val resultUser = MutableLiveData<Users?>()

    val progressBar = MutableLiveData<Boolean>()

    private lateinit var auth: FirebaseAuth

    init {
        checkUserLogin()
    }

    private fun checkUserLogin(){
        auth = Firebase.auth
        val currentUser = auth.currentUser
        if(currentUser != null){
            progressBar.postValue(true)
            checkUser.postValue(2)
            val user = Firebase.auth.currentUser
            user?.let {
                val uid: String = user.uid
                FirebaseDatabase.getInstance().getReference(Constants.Firebase.FIRE_BASE_USERS)
                    .child(uid)
                    .addValueEventListener(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            val users: Users? = dataSnapshot.getValue(Users::class.java)
                            if (users != null) {
                                resultUser.postValue(users)
                                progressBar.postValue(false)
                            }
                        }
                        override fun onCancelled(databaseError: DatabaseError) {}
                    })
            }
        } else {
            checkUser.postValue(1)
        }
    }
}
