package com.example.baseproject.ui.tabTrending

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.databinding.ItemTrendingBinding
import com.example.core.model.SongItem
import com.example.core.utils.listener.OnItemClickPopupAddSong
import java.util.*

class TrendingAdapter constructor(context: Context) :
    ListAdapter<SongItem, TrendingHolder>(TrendingDiffUtil()) {

    private var onItemClickPopupAddSong: OnItemClickPopupAddSong? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickPopupAddSong: OnItemClickPopupAddSong?) {
        this.onItemClickPopupAddSong = onItemClickPopupAddSong
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrendingHolder {
        return TrendingHolder(
            ItemTrendingBinding.inflate(layoutInflater, parent, false), onItemClickPopupAddSong
        )
    }

    override fun onBindViewHolder(holder: TrendingHolder, position: Int) {
        holder.bind(getItem(position), onItemClickPopupAddSong)
    }

    override fun submitList(list: List<SongItem>?) {
        super.submitList(list?.let { ArrayList(it) })
    }
}

class TrendingHolder(val binding: ItemTrendingBinding, onItemClickPopupAddSong: OnItemClickPopupAddSong?) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.itemTrending.setOnClickListener {
            onItemClickPopupAddSong?.setOnClick(adapterPosition)
        }
    }

    fun bind(list: SongItem, onItemClickPopupAddSong: OnItemClickPopupAddSong?) {
        Glide.with(itemView.context).load(list.image).into(binding.imgCircleTrending)
        binding.txtArtistTrending.text = list.artist
        binding.txtSongTrending.text = list.name

        val popupMenu = PopupMenu(itemView.context, binding.btnShowMenu)
        popupMenu.menuInflater.inflate(R.menu.menu_song_item, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            if (item.itemId == R.id.action_add_to_playlist) {
                onItemClickPopupAddSong?.setOnClickAddPlaylist(adapterPosition)
            } else if (item.itemId == R.id.action_add_to_favourite) {
                onItemClickPopupAddSong?.setOnClickAddFavourite(adapterPosition)
            }
            true
        }

        binding.btnShowMenu.setOnClickListener {
            popupMenu.show()
        }
    }
}

class TrendingDiffUtil : DiffUtil.ItemCallback<SongItem>() {
    override fun areItemsTheSame(oldItem: SongItem, newItem: SongItem): Boolean {
        return oldItem.id == newItem.id && oldItem.name == newItem.name
                && oldItem.artist == newItem.artist && oldItem.image == newItem.image
    }

    override fun areContentsTheSame(oldItem: SongItem, newItem: SongItem): Boolean {
        return oldItem == newItem
    }

}