package com.example.baseproject.ui.tabTrending

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.core.base.BaseViewModel
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TrendingViewModel @Inject constructor(private val trendingRepository: TrendingRepository) :
    BaseViewModel() {
    val listSongTrending = MutableLiveData<ArrayList<SongItem>>()
    private val arrayListTrending: ArrayList<SongItem> = ArrayList()

    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    val progressBar = MutableLiveData<Boolean>()

    init {
        getSongTrending()
    }

    private fun getSongTrending() {
        CoroutineScope(Dispatchers.IO).launch {
            progressBar.postValue(true)
            try {
                val responseSong = trendingRepository.getSongTrending()
                val responseArtist = trendingRepository.getArtist()
                for (i in responseSong.indices) {
                    for (j in responseArtist.indices) {
                        if (responseSong[i].artistId == responseArtist[j].id) {
                            val name: String? = responseSong[i].title
                            val image: String? = responseSong[i].image
                            val artist: String? = responseArtist[j].name
                            val source: String? = responseSong[i].source
                            val duration: Int? = responseSong[i].duration
                            arrayListTrending.add(SongItem(i, name, image, source, artist, duration))
                        }
                    }
                }

                listSongTrending.postValue(arrayListTrending)
                progressBar.postValue(false)
            } catch (e: Exception) {
                Log.getStackTraceString(e)
                listSongTrending.postValue(ArrayList())
            }
        }
    }

    fun addFavourite(songItem: SongItem) {
        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Firebase.REFERENCE_FAVOURITE)
                .child(firebaseUser.uid)
                .push()
                .setValue(songItem)
        }
    }
}
