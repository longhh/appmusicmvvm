package com.example.baseproject.ui.tabHomePage

import com.example.core.model.Album
import com.example.core.model.Artist
import com.example.core.model.Banner
import com.example.core.model.Genre
import com.example.core.network.RetrofitService
import javax.inject.Inject

class HomePageRepository @Inject constructor(private val retrofitService: RetrofitService){
    suspend fun getBanner(): java.util.ArrayList<Banner> = retrofitService.getBanner()

    suspend fun getAlbum(): ArrayList<Album> = retrofitService.getAlbum()

    suspend fun getArtist(): ArrayList<Artist> = retrofitService.getArtist()

    suspend fun getGenre(): ArrayList<Genre> = retrofitService.getGenre()
}