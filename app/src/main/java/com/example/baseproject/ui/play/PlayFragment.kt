package com.example.baseproject.ui.play

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.content.*
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.SeekBar
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentPlayBinding
import com.example.baseproject.service.MediaService
import com.example.core.base.BaseFragment
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.core.database.SQLiteFavourite
import com.example.core.utils.StringUtils.isIntegerNumber
import javax.inject.Inject

@AndroidEntryPoint
class PlayFragment :
    BaseFragment<FragmentPlayBinding, PlayViewModel>(R.layout.fragment_play) {

    private val viewModel: PlayViewModel by viewModels()

    override fun getVM(): PlayViewModel = viewModel

    @Inject
    lateinit var sqLiteFavourite: SQLiteFavourite

    private val intent by lazy {
        Intent(activity, MediaService::class.java)
    }
    private var mediaService: MediaService? = null

    private var serviceConnection = object: ServiceConnection{
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder: MediaService.MediaBinder = p1 as MediaService.MediaBinder
            mediaService = binder.getMediaService()
            setupServiceConnection()
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
        }
    }

    private fun setupServiceConnection() {
        viewModel.dataSong.value?.let { startMusic(it) }
        if (!mediaService!!.isPlaying) {
            binding.imgSongPlay.clearAnimation()
            binding.btnPlayOrStop.setImageResource(R.drawable.ic_play_circle)
        } else {
            binding.imgSongPlay.startAnimation(animation)
            binding.btnPlayOrStop.setImageResource(R.drawable.ic_pause_circle)
        }

        if (!mediaService!!.isLooping) {
            binding.btnRepeat.setImageResource(R.drawable.ic_repeat)
        } else {
            binding.btnRepeat.setImageResource(R.drawable.ic_repeat_white)
        }

        if (!mediaService!!.isRandom) {
            binding.btnRandom.setImageResource(R.drawable.ic_shuffle)
        } else {
            binding.btnRandom.setImageResource(R.drawable.ic_shuffle_white)
        }

        binding.btnPlayOrStop.setOnClickListener {
            if (isRunning) {
                if (mediaService!!.isPlaying) {
                    mediaService?.pauseMusic()
                    binding.imgSongPlay.clearAnimation()
                    binding.btnPlayOrStop.setImageResource(R.drawable.ic_play_circle)
                } else {
                    mediaService?.resumeMusic()
                    binding.imgSongPlay.startAnimation(animation)
                    binding.btnPlayOrStop.setImageResource(R.drawable.ic_pause_circle)
                }
            } else {
                if (viewModel.dataSong.value != null) {
                    startMusic(viewModel.dataSong.value!!)
                } else if (viewModel.songItem.value != null) {
                    startMusic(viewModel.songItem.value!!)
                }
                binding.imgSongPlay.startAnimation(animation)
                binding.btnPlayOrStop.setImageResource(R.drawable.ic_pause_circle)
                isRunning = true
            }
        }
        binding.btnRepeat.setOnClickListener {
            if (mediaService!!.isLooping) {
                mediaService?.isLooping = false
                binding.btnRepeat.setImageResource(R.drawable.ic_repeat)
            } else {
                mediaService?.isLooping = true
                binding.btnRepeat.setImageResource(R.drawable.ic_repeat_white)
            }
        }
        binding.btnRandom.setOnClickListener {
            if (mediaService!!.isRandom) {
                mediaService!!.isRandom = false
                binding.btnRandom.setImageResource(R.drawable.ic_shuffle)
            } else {
                mediaService!!.isRandom = true
                binding.btnRandom.setImageResource(R.drawable.ic_shuffle_white)
            }
        }

        binding.btnNextSong.setOnClickListener {
            mediaService?.nextMusic()
            binding.imgSongPlay.startAnimation(animation)
            binding.btnPlayOrStop.setImageResource(R.drawable.ic_pause_circle)
        }

        binding.btnBackSong.setOnClickListener {
            mediaService?.backMusic()
            binding.imgSongPlay.startAnimation(animation)
            binding.btnPlayOrStop.setImageResource(R.drawable.ic_pause_circle)
        }

        binding.seekBarSong.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                if(p2){
                    mediaService?.seekBarChange(p1 * 1000)
                    if (p1 == binding.seekBarSong.max) {
                        if (!mediaService!!.isLooping) {
                            mediaService?.nextMusic()
                        } else {
                            mediaService?.startMusic(viewModel.dataSong.value!![position])
                        }
                    }
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }
        })
        binding.imgTimer.setOnClickListener {
            dialogTimer()
        }
        viewModel.resultFavourite.observe(viewLifecycleOwner, { it1 ->
            binding.imgFavourite.setOnClickListener {
                if (it1) {
                    viewModel.removeFavourite(mediaService!!.filename.toString())
                    viewModel.resultFavourite.value = false
                    Glide.with(requireContext()).load(R.drawable.ic_favourite).into(binding.imgFavourite)
                } else {
                    viewModel.addFavourite(mediaService!!.song!!)
                    Glide.with(requireContext()).load(R.drawable.ic_favourite_checked).into(binding.imgFavourite)
                }
            }
            if (!it1) {
                mediaService!!.favourite = false
                Glide.with(requireContext()).load(R.drawable.ic_favourite).into(binding.imgFavourite)
            } else {
                mediaService!!.favourite = true
                Glide.with(requireContext()).load(R.drawable.ic_favourite_checked).into(binding.imgFavourite)
            }
        })
    }

    private val animation by lazy {
        AnimationUtils.loadAnimation(context, R.anim.disc_rotate)
    }
    private var position = 0
    private var isRunning = true
    private val booleanFromService = object: BroadcastReceiver() {
        @SuppressLint("SetTextI18n")
        override fun onReceive(p0: Context?, p1: Intent?) {
            val isServiceRunning: Boolean = p1?.getSerializableExtra(Constants.Bundle.BOOLEAN) as Boolean
            if (!isServiceRunning) {
                binding.imgSongPlay.clearAnimation()
                binding.btnPlayOrStop.setImageResource(R.drawable.ic_play_circle)
                binding.seekBarSong.progress = 0
                binding.txtTimeSong.text = "00:00"
                isRunning = false
            }
        }
    }
    private val dataSong = object: BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val songItem: SongItem = p1?.getSerializableExtra(Constants.Bundle.DATA_FROM_SERVICE) as SongItem
            if (mediaService?.isPause == true) {
                binding.imgSongPlay.clearAnimation()
                binding.btnPlayOrStop.setImageResource(R.drawable.ic_play_circle)
                mediaService?.isPause = false
            }
            if (mediaService?.isResume == true) {
                binding.imgSongPlay.startAnimation(animation)
                binding.btnPlayOrStop.setImageResource(R.drawable.ic_pause_circle)
                mediaService?.isResume = false
            }
            if (songItem.image!!.isIntegerNumber) {
                Glide.with(p0!!).load(songItem.image!!.toInt()).into(binding.imgSongPlay)
            } else {
                Glide.with(p0!!).load(songItem.image).into(binding.imgSongPlay)
            }
            binding.seekBarSong.max = songItem.duration!!
            binding.txtNameSongPlay.text = songItem.name
            binding.txtNameArtistPlay.text = songItem.artist
            binding.txtTimeTotal.text = songItem.duration?.let { it1 -> formatTime(it1) }
        }
    }
    private val song = object: BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val songItem: SongItem = p1?.getSerializableExtra(Constants.Bundle.DATA_FROM_SERVICE) as SongItem
            viewModel.getDataFavourite(songItem.name.toString())
        }
    }
    private val mediaPosition = object: BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1 != null) {
                val mediaPosition: Int = p1.getIntExtra(Constants.Bundle.MEDIA_POSITION_FROM_SERVICE, 0)
                binding.seekBarSong.progress = mediaPosition
                binding.txtTimeSong.text = formatTime(mediaPosition)
                if (mediaPosition == binding.seekBarSong.max) {
                    if (!mediaService!!.isLooping) {
                        mediaService?.nextMusic()
                    } else {
                        mediaService?.startMusic(viewModel.dataSong.value!![position])
                    }
                }
            }
        }
    }
    private val timer = object: BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1 != null) {
                val time: Int = p1.getIntExtra(Constants.Bundle.TIMER, 0)
                if (mediaService!!.isTimer) {
                    binding.txtTimeCountdown.text = formatTime(time)
                    binding.txtTimeCountdown.visibility = View.VISIBLE
                    if (time < 0) {
                        mediaService!!.isTimer = false
                        binding.txtTimeCountdown.text = formatTime(0)
                        binding.txtTimeCountdown.visibility = View.GONE
                        mediaService?.pauseMusic()
                        binding.imgSongPlay.clearAnimation()
                        binding.btnPlayOrStop.setImageResource(R.drawable.ic_play_circle)
                    }
                }
            }
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        activity?.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(timer, IntentFilter(Constants.ActionIntent.TIMER))
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(song, IntentFilter(Constants.ActionIntent.SONG))
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(dataSong, IntentFilter(Constants.ActionIntent.SONG_UPDATE))
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(booleanFromService, IntentFilter(Constants.ActionIntent.BOOLEAN))
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(mediaPosition, IntentFilter(Constants.ActionIntent.MEDIA_POSITION))
    }


    override fun setOnClick() {
        super.setOnClick()
        binding.imgBack.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    override fun bindingStateView() {
        super.bindingStateView()
        viewModel.dataSong.observe(viewLifecycleOwner, {
            if (it != null) {
                position = viewModel.position.value!!
                if (it[position].image!!.isIntegerNumber) {
                    Glide.with(requireContext()).load(it[position].image!!.toInt()).into(binding.imgSongPlay)
                } else {
                    Glide.with(requireContext()).load(it[position].image).into(binding.imgSongPlay)
                }
                binding.seekBarSong.max = it[position].duration!!
                binding.txtNameSongPlay.text = it[position].name
                binding.txtNameArtistPlay.text = it[position].artist
                binding.txtTimeTotal.text = it[position].duration?.let { it1 -> formatTime(it1) }
            }
        })
        activity?.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        viewModel.songItem.observe(viewLifecycleOwner, {
            if (it != null) {
                position = viewModel.position.value!!
                if (it[position].image!!.isIntegerNumber) {
                    Glide.with(requireContext()).load(it[position].image!!.toInt()).into(binding.imgSongPlay)
                } else {
                    Glide.with(requireContext()).load(it[position].image).into(binding.imgSongPlay)
                }
                binding.seekBarSong.max = it[position].duration!!
                binding.txtNameSongPlay.text = it[position].name
                binding.txtNameArtistPlay.text = it[position].artist
                binding.txtTimeTotal.text = it[position].duration?.let { it1 -> formatTime(it1) }
            }
        })
    }

    private fun startMusic(arrSong: ArrayList<SongItem>) {
        val bundle = Bundle()
        bundle.putSerializable(Constants.Bundle.SONG_ITEM, arrSong)
        bundle.putInt(Constants.Bundle.POSITION, position)
        intent.putExtras(bundle)
        mediaService?.isStart = true
        activity?.startService(intent)
    }

    private fun formatTime(pTime: Int): String {
        val min = pTime / 60
        val sec = pTime - min * 60
        val strMin: String = placeZeroIfNeed(min)
        val strSec: String = placeZeroIfNeed(sec)
        return String.format("$strMin:$strSec")
    }
    private fun placeZeroIfNeed(number: Int): String {
        return if (number >= 10) {
            number.toString()
        } else {
            String.format("0$number")
        }
    }

    private fun dialogTimer() {
        val timePickerDialog = TimePickerDialog(requireContext(),
            android.R.style.Theme_Holo_Dialog_MinWidth, { _, p1, p2 ->
                if (!mediaService!!.isTimer) {
                    mediaService!!.isTimer = true
                    mediaService!!.timer = p1 * 3600 + p2 * 60
                }
            }, 24, 0, true)
        timePickerDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        timePickerDialog.updateTime(0,0)
        timePickerDialog.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(timer)
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(song)
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(dataSong)
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(booleanFromService)
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(mediaPosition)
        if (isRunning) {
            activity?.unbindService(serviceConnection)
        }
    }
}