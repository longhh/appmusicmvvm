package com.example.baseproject.ui.tabHomePage

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.example.baseproject.databinding.ItemAlbumBinding
import com.example.core.model.Album
import com.example.core.utils.listener.OnItemClickListener
import java.util.*
import com.bumptech.glide.load.resource.bitmap.RoundedCorners


class AlbumHomePageAdapter constructor(context: Context) :
    ListAdapter<Album, AlbumHomePageHolder>(AlbumHomePageDiffUtil()) {

    private var onItemClickListener: OnItemClickListener? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        this.onItemClickListener = onItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumHomePageHolder {
        return AlbumHomePageHolder(
            ItemAlbumBinding.inflate(layoutInflater, parent, false), onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: AlbumHomePageHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun submitList(list: List<Album>?) {
        super.submitList(list?.let { ArrayList(it) })
    }
}

class AlbumHomePageHolder(
    val binding: ItemAlbumBinding,
    onItemClickListener: OnItemClickListener?
) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.itemAlbum.setOnClickListener {
            onItemClickListener?.setOnClick(adapterPosition)
        }
    }

    @SuppressLint("CheckResult")
    fun bind(list: Album) {
        Glide.with(itemView.context).load(list.image)
            .transform(CenterCrop(), RoundedCorners(20))
            .into(binding.imageAlbum)
        binding.titleAlbum.text = list.name
    }
}

class AlbumHomePageDiffUtil : DiffUtil.ItemCallback<Album>() {
    override fun areItemsTheSame(oldItem: Album, newItem: Album): Boolean {
        return oldItem.name == newItem.name && oldItem.id == newItem.id
                && oldItem.image == newItem.image && oldItem.totalSong == newItem.totalSong
    }

    override fun areContentsTheSame(oldItem: Album, newItem: Album): Boolean {
        return oldItem == newItem
    }

}