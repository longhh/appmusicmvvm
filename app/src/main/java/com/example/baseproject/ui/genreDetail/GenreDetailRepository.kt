package com.example.baseproject.ui.genreDetail

import com.example.core.model.Artist
import com.example.core.model.Song
import com.example.core.network.RetrofitService
import javax.inject.Inject

class GenreDetailRepository @Inject constructor(private val retrofitService: RetrofitService){
    suspend fun getSong(): ArrayList<Song> = retrofitService.getSong()

    suspend fun getArtist(): ArrayList<Artist> = retrofitService.getArtist()
}