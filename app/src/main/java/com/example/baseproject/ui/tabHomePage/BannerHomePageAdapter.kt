package com.example.baseproject.ui.tabHomePage

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.example.baseproject.databinding.ItemBannerBinding
import com.example.core.model.Banner
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.R


class BannerHomePageAdapter(private val context: Context, private var listBanner: ArrayList<Banner>) :
    PagerAdapter() {

    fun setData(listBanner: ArrayList<Banner>?) {
        if (listBanner != null) {
            this.listBanner = listBanner
        }
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return listBanner.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    @SuppressLint("InflateParams")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(context)
        val binding: ItemBannerBinding =
            DataBindingUtil.inflate(inflater, R.layout.item_banner, container, false)
        Glide.with(context).load(listBanner[position].imageArtist)
            .transform(CenterCrop(), RoundedCorners(10))
            .into(binding.imageBannerArtist)
        Glide.with(context).load(listBanner[position].image).into(binding.imageBanner)
        binding.txtArtist.text = listBanner[position].artist
        binding.txtTitleBanner.text = listBanner[position].title

        container.addView(binding.root)
        return binding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}