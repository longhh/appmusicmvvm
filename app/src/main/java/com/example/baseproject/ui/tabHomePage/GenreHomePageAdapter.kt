package com.example.baseproject.ui.tabHomePage

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.databinding.ItemGenreBinding
import com.example.core.model.Genre
import com.example.core.utils.listener.OnItemClickListener
import java.util.*

class GenreHomePageAdapter constructor(context: Context) :
    ListAdapter<Genre, GenreHomePageHolder>(GenreHomePageDiffUtil()) {

    private var onItemClickListener: OnItemClickListener? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        this.onItemClickListener = onItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreHomePageHolder {
        return GenreHomePageHolder(
            ItemGenreBinding.inflate(layoutInflater, parent, false), onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: GenreHomePageHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun submitList(list: List<Genre>?) {
        super.submitList(list?.let { ArrayList(it) })
    }
}

class GenreHomePageHolder(val binding: ItemGenreBinding, onItemClickListener: OnItemClickListener?) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.itemGenre.setOnClickListener {
            onItemClickListener?.setOnClick(adapterPosition)
        }
    }

    @SuppressLint("CheckResult")
    fun bind(list: Genre) {
        Glide.with(itemView.context).load(list.image)
            .transform(CenterCrop(), RoundedCorners(20))
            .into(binding.imageGenre)
        binding.titleGenre.text = list.name
    }
}

class GenreHomePageDiffUtil : DiffUtil.ItemCallback<Genre>() {
    override fun areItemsTheSame(oldItem: Genre, newItem: Genre): Boolean {
        return oldItem.name == newItem.name && oldItem.id == newItem.id
                && oldItem.image == newItem.image
    }

    override fun areContentsTheSame(oldItem: Genre, newItem: Genre): Boolean {
        return oldItem == newItem
    }

}