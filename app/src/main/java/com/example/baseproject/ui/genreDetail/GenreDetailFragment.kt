package com.example.baseproject.ui.genreDetail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentGenreDetailBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.database.SQLitePlaylistDetail
import com.example.core.utils.Constants
import com.example.core.utils.listener.OnItemClickPopupAddSong
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class GenreDetailFragment :
    BaseFragment<FragmentGenreDetailBinding, GenreDetailViewModel>(R.layout.fragment_genre_detail) {

    private val viewModel: GenreDetailViewModel by viewModels()

    override fun getVM(): GenreDetailViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var sqLitePlaylistDetail: SQLitePlaylistDetail

    private val adapterGenreDetail: GenreDetailAdapter by lazy {
        GenreDetailAdapter(requireContext())
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        showHideLoading(true)
        setupAdapter()
        setupView()
    }

    private fun setupAdapter() {
        binding.rvSongGenre.adapter = adapterGenreDetail
    }

    private fun setupView() {
        binding.btnGenreBackHomepage.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    override fun bindingStateView() {
        super.bindingStateView()
        viewModel.progressBar.observe(viewLifecycleOwner, {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })
        viewModel.listSongGenre.observe(viewLifecycleOwner, {
            adapterGenreDetail.submitList(it)
            adapterGenreDetail.setOnItemClickListener(object : OnItemClickPopupAddSong {
                override fun setOnClick(position: Int) {
                    val bundle = Bundle()
                    val songItem = viewModel.listSongGenre.value
                    bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
                    bundle.putInt(Constants.Bundle.POSITION, position)
                    appNavigation.openGenreDetailToPlayScreen(bundle)
                }

                override fun setOnClickAddPlaylist(position: Int) {
                    sqLitePlaylistDetail.insertDataPlaylistDetail(
                        it[position].name,
                        it[position].image,
                        it[position].source,
                        it[position].artist,
                        it[position].duration
                    )
                    Toast.makeText(context, getString(R.string.added_to_playlist), Toast.LENGTH_SHORT).show()
                }

                override fun setOnClickAddFavourite(position: Int) {
                    viewModel.addFavourite(it[position])
                    Toast.makeText(context, getString(R.string.added_to_favourite), Toast.LENGTH_SHORT).show()
                }
            })
            binding.btnPlayGenre.setOnClickListener {
                val bundle = Bundle()
                val songItem = viewModel.listSongGenre.value
                bundle.putSerializable(Constants.Bundle.SONG_ITEM, songItem)
                bundle.putInt(Constants.Bundle.POSITION, 0)
                appNavigation.openGenreDetailToPlayScreen(bundle)
            }
        })

        viewModel.imageGenreLive.observe(viewLifecycleOwner, {
            Glide.with(requireContext()).load(it)
                .transform(CenterCrop(), RoundedCorners(20))
                .into(binding.imgGenreDetail)
        })

        viewModel.nameGenreLive.observe(viewLifecycleOwner, {
            binding.txtTitleGenreDetail.text = it
        })
    }
}