package com.example.baseproject.ui.personalMusic

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.baseproject.R
import com.example.core.base.BaseViewModel
import com.example.core.model.SongItem
import com.example.core.model.SongPersonal
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PersonalMusicViewModel @Inject constructor() : BaseViewModel() {
    val listDataPersonalMusic = MutableLiveData<ArrayList<SongPersonal>>()
    val listDataSongItem = MutableLiveData<ArrayList<SongItem>>()

    fun getPersonalMusic(context: Context) {
        val songList = ArrayList<SongPersonal>()
        val songItem = ArrayList<SongItem>()
        val audioCursor: Cursor? = context.contentResolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null, null
        )
        if (audioCursor != null) {
            if (audioCursor.moveToFirst()) {
                do {
                    val audioTitle = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE)
                    val audioArtist = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST)
                    val audioDuration = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)
                    val audioData = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)
                    val audioAlbum = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM)
                    val audioAlbumId = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID)
                    val songId = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID)

                    val info = SongPersonal()
                    info.source = audioCursor.getString(audioData)
                    info.name = audioCursor.getString(audioTitle)
                    info.duration = audioCursor.getLong(audioDuration) / 1000
                    info.artist = audioCursor.getString(audioArtist)
                    info.album = audioCursor.getString(audioAlbum)
                    info.id = audioCursor.getLong(songId)
                    info.albumId = audioCursor.getLong(audioAlbumId)

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        val albumArtUri = Uri.parse("content://media/external/audio/albumart")
                        info.image = ContentUris.withAppendedId(albumArtUri, info.albumId!!).toString()
                    } else {
//                        if (audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART) != null) {
//                            info.image = audioCursor.getString(audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART))
//                        } else {
                            info.image = R.drawable.ic_launcher_foreground.toString()
//                        }
                    }
                    if (!info.name.equals("")) {
                        songList.add(info)
                        songItem.add(SongItem(info.id!!.toInt(), info.name, info.image, info.source, info.artist, info.duration!!.toInt()))
                    }
                } while (audioCursor.moveToNext())
            }
        }
        audioCursor?.close()
        listDataPersonalMusic.postValue(songList)
        listDataSongItem.postValue(songItem)
    }
}
