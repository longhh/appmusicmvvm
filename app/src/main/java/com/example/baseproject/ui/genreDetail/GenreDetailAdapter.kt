package com.example.baseproject.ui.genreDetail

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.databinding.ItemGenreDetailBinding
import com.example.core.model.SongItem
import com.example.core.utils.listener.OnItemClickPopupAddSong
import java.util.*

class GenreDetailAdapter constructor(context: Context) :
    ListAdapter<SongItem, GenreDetailHolder>(GenreDetailDiffUtil()) {

    private var onItemClickPopupAddSong: OnItemClickPopupAddSong? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickPopupAddSong: OnItemClickPopupAddSong?) {
        this.onItemClickPopupAddSong = onItemClickPopupAddSong
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreDetailHolder {
        return GenreDetailHolder(
            ItemGenreDetailBinding.inflate(layoutInflater, parent, false), onItemClickPopupAddSong
        )
    }

    override fun onBindViewHolder(holder: GenreDetailHolder, position: Int) {
        holder.bind(getItem(position), onItemClickPopupAddSong)
    }

    override fun submitList(list: List<SongItem>?) {
        super.submitList(list?.let { ArrayList(it) })
    }
}

class GenreDetailHolder(
    val binding: ItemGenreDetailBinding,
    onItemClickPopupAddSong: OnItemClickPopupAddSong?
) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.itemGenreDetail.setOnClickListener {
            onItemClickPopupAddSong?.setOnClick(adapterPosition)
        }
    }

    fun bind(list: SongItem, onItemClickPopupAddSong: OnItemClickPopupAddSong?) {
        Glide.with(itemView.context).load(list.image).into(binding.imgCircleGenreDetail)
        binding.txtSongGenreDetail.text = list.name
        binding.txtArtistGenre.text = list.artist

        val popupMenu = PopupMenu(itemView.context, binding.btnShowMenu)
        popupMenu.menuInflater.inflate(R.menu.menu_song_item, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            if (item.itemId == R.id.action_add_to_playlist) {
                onItemClickPopupAddSong?.setOnClickAddPlaylist(adapterPosition)
            } else if (item.itemId == R.id.action_add_to_favourite) {
                onItemClickPopupAddSong?.setOnClickAddFavourite(adapterPosition)
            }
            true
        }

        binding.btnShowMenu.setOnClickListener {
            popupMenu.show()
        }
    }
}

class GenreDetailDiffUtil : DiffUtil.ItemCallback<SongItem>() {
    override fun areItemsTheSame(oldItem: SongItem, newItem: SongItem): Boolean {
        return oldItem.id == newItem.id && oldItem.name == newItem.name && oldItem.source == newItem.source
                && oldItem.image == newItem.image && oldItem.source == newItem.source
    }

    override fun areContentsTheSame(oldItem: SongItem, newItem: SongItem): Boolean {
        return oldItem == newItem
    }

}