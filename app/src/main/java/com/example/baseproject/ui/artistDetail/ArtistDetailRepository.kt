package com.example.baseproject.ui.artistDetail

import com.example.core.model.Song
import com.example.core.network.RetrofitService
import javax.inject.Inject

class ArtistDetailRepository @Inject constructor(private val retrofitService: RetrofitService){
    suspend fun getSong(): ArrayList<Song> = retrofitService.getSong()
}