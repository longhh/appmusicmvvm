package com.example.baseproject.ui.playlistDetail

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.core.model.SongItem
import java.util.*
import android.widget.PopupMenu
import com.example.baseproject.R
import com.example.baseproject.databinding.ItemPlaylistDetailBinding
import com.example.core.utils.listener.OnItemClickPopupMenu


class PlaylistDetailAdapter constructor(context: Context) :
    ListAdapter<SongItem, PlaylistDetailHolder>(PlaylistDetailDiffUtil()) {

    private var onItemClickPopupMenu: OnItemClickPopupMenu? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickPopupMenu: OnItemClickPopupMenu?) {
        this.onItemClickPopupMenu = onItemClickPopupMenu
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistDetailHolder {
        return PlaylistDetailHolder(
            ItemPlaylistDetailBinding.inflate(layoutInflater, parent, false), onItemClickPopupMenu
        )
    }

    override fun onBindViewHolder(holder: PlaylistDetailHolder, position: Int) {
        holder.bind(getItem(position), onItemClickPopupMenu)
    }

    override fun submitList(list: List<SongItem>?) {
        super.submitList(list?.let { ArrayList(it) })
    }
}

class PlaylistDetailHolder(val binding: ItemPlaylistDetailBinding, onItemClickListener: OnItemClickPopupMenu?) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.itemPlaylistDetail.setOnClickListener {
            onItemClickListener?.setOnClick(adapterPosition)
        }
    }

    fun bind(list: SongItem, onItemClickListener: OnItemClickPopupMenu?) {
        Glide.with(itemView.context).load(list.image).into(binding.imgCirclePlaylistDetail)
        binding.txtSongPlaylistDetail.text = list.name
        binding.txtArtistPlaylist.text = list.artist

        val popupMenu = PopupMenu(itemView.context, binding.btnShowMenu)
        popupMenu.menuInflater.inflate(R.menu.menu_playlist_item, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            if (item.itemId == R.id.action_delete) {
                onItemClickListener?.setOnClickPopupMenu(adapterPosition)
            }
            true
        }

        binding.btnShowMenu.setOnClickListener {
            popupMenu.show()
        }
    }
}

class PlaylistDetailDiffUtil : DiffUtil.ItemCallback<SongItem>() {
    override fun areItemsTheSame(oldItem: SongItem, newItem: SongItem): Boolean {
        return oldItem.id == newItem.id && oldItem.name == newItem.name
                && oldItem.source == newItem.source && oldItem.image == newItem.image
                && oldItem.artist == newItem.artist
    }

    override fun areContentsTheSame(oldItem: SongItem, newItem: SongItem): Boolean {
        return oldItem == newItem
    }

}