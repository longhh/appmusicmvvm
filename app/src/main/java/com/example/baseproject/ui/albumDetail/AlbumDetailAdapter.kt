package com.example.baseproject.ui.albumDetail

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.baseproject.databinding.ItemAlbumDetailBinding
import com.example.core.model.SongItem
import java.util.*
import android.widget.PopupMenu
import com.example.baseproject.R
import com.example.core.utils.listener.OnItemClickPopupAddSong


class AlbumDetailAdapter constructor(context: Context) :
    ListAdapter<SongItem, AlbumDetailHolder>(AlbumDetailDiffUtil()) {

    private var onItemClickPopupAddSong: OnItemClickPopupAddSong? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickPopupAddSong: OnItemClickPopupAddSong?) {
        this.onItemClickPopupAddSong = onItemClickPopupAddSong
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumDetailHolder {
        return AlbumDetailHolder(
            ItemAlbumDetailBinding.inflate(layoutInflater, parent, false), onItemClickPopupAddSong
        )
    }

    override fun onBindViewHolder(holder: AlbumDetailHolder, position: Int) {
        holder.bind(getItem(position), onItemClickPopupAddSong)
    }

    override fun submitList(list: List<SongItem>?) {
        super.submitList(list?.let { ArrayList(it) })
    }
}

class AlbumDetailHolder(val binding: ItemAlbumDetailBinding, onItemClickPopupAddSong: OnItemClickPopupAddSong?) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.itemAlbumDetail.setOnClickListener {
            onItemClickPopupAddSong?.setOnClick(adapterPosition)
        }
    }

    fun bind(list: SongItem, onItemClickPopupAddSong: OnItemClickPopupAddSong?) {
        Glide.with(itemView.context).load(list.image).into(binding.imgCircleAlbumDetail)
        binding.txtSongAlbumDetail.text = list.name
        binding.txtArtistAlbum.text = list.artist

        val popupMenu = PopupMenu(itemView.context, binding.btnShowMenu)
        popupMenu.menuInflater.inflate(R.menu.menu_song_item, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            if (item.itemId == R.id.action_add_to_playlist) {
                onItemClickPopupAddSong?.setOnClickAddPlaylist(adapterPosition)
            } else if (item.itemId == R.id.action_add_to_favourite) {
                onItemClickPopupAddSong?.setOnClickAddFavourite(adapterPosition)
            }
            true
        }

        binding.btnShowMenu.setOnClickListener {
            popupMenu.show()
        }
    }
}

class AlbumDetailDiffUtil : DiffUtil.ItemCallback<SongItem>() {
    override fun areItemsTheSame(oldItem: SongItem, newItem: SongItem): Boolean {
        return oldItem.id == newItem.id && oldItem.name == newItem.name
                && oldItem.source == newItem.source && oldItem.image == newItem.image
                && oldItem.artist == newItem.artist
    }

    override fun areContentsTheSame(oldItem: SongItem, newItem: SongItem): Boolean {
        return oldItem == newItem
    }
}