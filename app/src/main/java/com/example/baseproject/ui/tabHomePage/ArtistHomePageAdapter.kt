package com.example.baseproject.ui.tabHomePage

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.databinding.ItemArtistBinding
import com.example.core.model.Artist
import com.example.core.utils.listener.OnItemClickListener
import java.util.*

class ArtistHomePageAdapter constructor(context: Context) :
    ListAdapter<Artist, ArtistHomePageHolder>(ArtistHomePageDiffUtil()) {

    private var onItemClickListener: OnItemClickListener? = null

    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        this.onItemClickListener = onItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistHomePageHolder {
        return ArtistHomePageHolder(
            ItemArtistBinding.inflate(layoutInflater, parent, false), onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: ArtistHomePageHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun submitList(list: List<Artist>?) {
        super.submitList(list?.let { ArrayList(it) })
    }
}

class ArtistHomePageHolder(
    val binding: ItemArtistBinding,
    onItemClickListener: OnItemClickListener?
) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.itemArtist.setOnClickListener {
            onItemClickListener?.setOnClick(adapterPosition)
        }
    }

    @SuppressLint("CheckResult")
    fun bind(list: Artist) {
        Glide.with(itemView.context).load(list.image)
            .transform(CenterCrop(), RoundedCorners(20))
            .into(binding.imageArtist)
        binding.titleArtist.text = list.name
    }
}

class ArtistHomePageDiffUtil : DiffUtil.ItemCallback<Artist>() {
    override fun areItemsTheSame(oldItem: Artist, newItem: Artist): Boolean {
        return oldItem.name == newItem.name && oldItem.id == newItem.id
                && oldItem.image == newItem.image
    }

    override fun areContentsTheSame(oldItem: Artist, newItem: Artist): Boolean {
        return oldItem == newItem
    }

}