package com.example.baseproject.ui.home

import android.annotation.SuppressLint
import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentHomeBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.baseproject.service.MediaService
import com.example.core.base.BaseFragment
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import com.example.core.utils.StringUtils.isIntegerNumber
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(R.layout.fragment_home) {

    private var pos: Int? = null

    @Inject
    lateinit var appNavigation: AppNavigation

    override fun getVM(): HomeViewModel = viewModel

    private val intent by lazy {
        Intent(activity, MediaService::class.java)
    }
    private var mediaService: MediaService? = null
    private var song: SongItem? = null
    private var arr: ArrayList<SongItem>? = null
    private var serviceConnection = object: ServiceConnection{
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder: MediaService.MediaBinder = p1 as MediaService.MediaBinder
            mediaService = binder.getMediaService()
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
        }
    }
    private val booleanFromService = object: BroadcastReceiver() {
        @SuppressLint("SetTextI18n")
        override fun onReceive(p0: Context?, p1: Intent?) {
            val isServiceRunning: Boolean = p1?.getSerializableExtra(Constants.Bundle.BOOLEAN) as Boolean
            if (!isServiceRunning) {
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_play_white)
                binding.progressBar.progress = 0
                binding.progressBar.visibility = View.GONE
                binding.layoutMusicControlBar.visibility = View.GONE
            }
        }
    }
    private val viewModel: HomeViewModel by viewModels()
    private val dataSong = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val songItem: SongItem = p1?.getSerializableExtra(Constants.Bundle.DATA_FROM_SERVICE) as SongItem
            song = songItem
            if (mediaService?.isPause == true) {
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_play_white)
                mediaService?.isPause = false
            }
            if (mediaService?.isResume == true) {
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_pause_white)
                mediaService?.isResume = false
            }
            if (!mediaService!!.isPlaying) {
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_play_white)
            } else {
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_pause_white)
            }
            if (!mediaService!!.favourite) {
                Glide.with(requireContext()).load(R.drawable.ic_favourite).into(binding.btnFavourite)
            } else {
                Glide.with(requireContext()).load(R.drawable.ic_favourite_checked).into(binding.btnFavourite)
            }
            if (songItem.image!!.isIntegerNumber) {
                Glide.with(p0!!).load(songItem.image!!.toInt()).into(binding.imgMusicControl)
            } else {
                Glide.with(p0!!).load(songItem.image).into(binding.imgMusicControl)
            }
            binding.progressBar.max = songItem.duration!!
            binding.txtSongMusicControl.text = songItem.name
            binding.txtArtistMusicControl.text = songItem.artist
            binding.progressBar.visibility = View.VISIBLE
            binding.layoutMusicControlBar.visibility = View.VISIBLE
        }
    }
    private val mediaPosition = object: BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1 != null) {
                val mediaPosition: Int = p1.getIntExtra(Constants.Bundle.MEDIA_POSITION_FROM_SERVICE, 0)
                binding.progressBar.progress = mediaPosition
                if (mediaPosition == binding.progressBar.max) {
                    if (!mediaService?.mediaPlayer!!.isLooping) {
                        mediaService!!.nextMusic()
                    } else {
                        mediaService!!.startMusic(song)
                    }
                }
            }
        }
    }
    private val songData = object: BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val songItem: SongItem = p1?.getSerializableExtra(Constants.Bundle.DATA_FROM_SERVICE) as SongItem
            viewModel.getDataFavourite(songItem.name.toString())
        }
    }
    private val listSong = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val list: ArrayList<SongItem> = p1?.getSerializableExtra(Constants.Bundle.LIST_SONG) as ArrayList<SongItem>
            val position: Int = p1.getSerializableExtra(Constants.Bundle.POSITION) as Int
            arr = list
            pos = position
        }
    }
    private val timer = object: BroadcastReceiver()
    {
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1 != null) {
                val time: Int = p1.getIntExtra(Constants.Bundle.TIMER, 0)
                if (mediaService!!.isTimer) {
                    if (time < 0) {
                        mediaService!!.isTimer = false
                        mediaService?.pauseMusic()
                    }
                }
            }
        }
    }
    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupBottomNavigationBar()
        activity?.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(timer, IntentFilter(Constants.ActionIntent.TIMER))
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(listSong, IntentFilter(Constants.ActionIntent.LIST_SONG))
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(songData, IntentFilter(Constants.ActionIntent.SONG))
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(dataSong, IntentFilter(Constants.ActionIntent.SONG_UPDATE))
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(booleanFromService, IntentFilter(Constants.ActionIntent.BOOLEAN))
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(mediaPosition, IntentFilter(Constants.ActionIntent.MEDIA_POSITION))
    }

    override fun setOnClick() {
        super.setOnClick()
        binding.btnPlayOrPause.setOnClickListener {
            if (mediaService!!.isPlaying) {
                mediaService!!.pauseMusic()
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_play_white)
            } else {
                mediaService!!.resumeMusic()
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_pause_white)
            }
        }
        binding.btnNextSong.setOnClickListener {
            mediaService!!.nextMusic()
            binding.btnPlayOrPause.setImageResource(R.drawable.ic_pause_white)
        }
        binding.layoutMusicControlBar.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable(Constants.Bundle.SONG_ITEM_FROM_CONTROL_BAR, arr)
            bundle.putSerializable(Constants.Bundle.POSITION, pos)
            appNavigation.openControlBarToPlayScreen(bundle)
        }
    }

    private fun setupBottomNavigationBar() {
        val navHostFragment = childFragmentManager.findFragmentById(R.id.nav_host_container) as NavHostFragment
        val navController = navHostFragment.navController
        binding.bottomNav.setupWithNavController(navController)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(timer)
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(listSong)
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(songData)
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(dataSong)
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(booleanFromService)
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(mediaPosition)
    }
}