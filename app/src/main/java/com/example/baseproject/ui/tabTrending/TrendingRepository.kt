package com.example.baseproject.ui.tabTrending

import com.example.core.model.Artist
import com.example.core.model.Song
import com.example.core.network.RetrofitService
import javax.inject.Inject

class TrendingRepository @Inject constructor(private val retrofitService: RetrofitService){
    suspend fun getSongTrending(): ArrayList<Song> = retrofitService.getTrending()

    suspend fun getArtist(): ArrayList<Artist> = retrofitService.getArtist()
}