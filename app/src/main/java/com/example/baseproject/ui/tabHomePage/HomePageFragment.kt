package com.example.baseproject.ui.tabHomePage

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentHomePageBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.utils.Constants
import com.example.core.utils.listener.OnItemClickHomePage
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomePageFragment :
    BaseFragment<FragmentHomePageBinding, HomePageViewModel>(R.layout.fragment_home_page) {

    private val viewModel: HomePageViewModel by viewModels()

    override fun getVM(): HomePageViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    private val homePageAdapter: HomePageAdapter by lazy {
        HomePageAdapter(requireContext())
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupAdapter()
    }

    private fun setupAdapter() {
        binding.rvHomePage.adapter = homePageAdapter
    }

    override fun bindingStateView() {
        super.bindingStateView()
        viewModel.progressBar.observe(viewLifecycleOwner, {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })
        viewModel.listDataHomePage.observe(viewLifecycleOwner, {
            homePageAdapter.submitList(it)
            homePageAdapter.setOnItemClickListener(object : OnItemClickHomePage {
                override fun setOnClick(position: Int, type: Int) {
                    when (type) {
                        1 -> {
                            val bundleAlbum = Bundle()
                            val album = viewModel.listDataAlbum.value?.get(position)
                            bundleAlbum.putSerializable(Constants.Bundle.ALBUM, album)
                            appNavigation.openTabHomePageToAlbumDetail(bundleAlbum)
                        }
                        2 -> {
                            val bundleArtist = Bundle()
                            val artist = viewModel.listDataArtist.value?.get(position)
                            bundleArtist.putSerializable(Constants.Bundle.ARTIST, artist)
                            appNavigation.openTabHomePageToArtistDetail(bundleArtist)
                        }
                        3 -> {
                            val bundleGenre = Bundle()
                            val genre = viewModel.listDataGenre.value?.get(position)
                            bundleGenre.putSerializable(Constants.Bundle.GENRE, genre)
                            appNavigation.openTabHomePageToGenreDetail(bundleGenre)
                        }
                    }
                }
            })
        })
    }
}