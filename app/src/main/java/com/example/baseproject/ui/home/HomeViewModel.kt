package com.example.baseproject.ui.home

import androidx.lifecycle.MutableLiveData
import com.example.core.base.BaseViewModel
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor() : BaseViewModel() {
    val resultFavourite = MutableLiveData(false)

    var result = false
    private var favourite: ArrayList<SongItem> = ArrayList()
    private val firebaseUser = FirebaseAuth.getInstance().currentUser

    fun getDataFavourite(nameSong: String?) {
        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Firebase.REFERENCE_FAVOURITE)
                .child(firebaseUser.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        favourite = ArrayList()
                        for (snapshot in dataSnapshot.children) {
                            val song = snapshot.getValue(SongItem::class.java)
                            if (song != null) {
                                if (song.name.toString() == nameSong) {
                                    result = true
                                }
                            }
                        }
                        if (result) {
                            resultFavourite.postValue(true)
                            result = false
                        } else {
                            resultFavourite.postValue(false)
                        }
                    }
                    override fun onCancelled(databaseError: DatabaseError) {
                    }
                })
        }
    }
}
