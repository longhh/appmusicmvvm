package com.example.baseproject.ui.register

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentRegisterBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RegisterFragment : BaseFragment<FragmentRegisterBinding, RegisterViewModel>(R.layout.fragment_register) {

    @Inject
    lateinit var appNavigation: AppNavigation

    private val viewModel: RegisterViewModel by viewModels()
    override fun getVM() = viewModel

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupListener()
        setupObserver()
        changeColorButton()
    }

    private fun setupListener() {
        binding.btnRegister.setOnClickListener {
            val username: String = binding.edtRegisterName.text.trim().toString()
            val email: String = binding.edtRegisterEmail.text.trim().toString()
            val pass: String = binding.edtRegisterPassword.text.trim().toString()
            viewModel.register(username, email, pass)
        }

        binding.imgBack.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }

        binding.txtBack.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    private fun setupObserver() {
        viewModel.resultData.observe(viewLifecycleOwner, {
            it?.let {
                if (it == 1) {
                    Toast.makeText(activity, R.string.register_success, Toast.LENGTH_SHORT).show()
                    val intent = requireActivity().intent
                    requireActivity().finish()
                    startActivity(intent)
                } else if (it == 2) {
                    Toast.makeText(activity, R.string.empty_details, Toast.LENGTH_SHORT).show()
                }
            }
        })

    }

    private fun changeColorButton() {
        binding.edtRegisterName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtRegisterName.text!!.isNotEmpty()
                    && binding.edtRegisterEmail.text!!.isNotEmpty()
                    && binding.edtRegisterPassword.text!!.isNotEmpty()
                    && binding.checkboxRegister.isChecked
                ) {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button)
                } else {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button_gray)
                }
            }
        })
        binding.edtRegisterEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtRegisterName.text!!.isNotEmpty()
                    && binding.edtRegisterEmail.text!!.isNotEmpty()
                    && binding.edtRegisterPassword.text!!.isNotEmpty()
                    && binding.checkboxRegister.isChecked
                ) {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button)
                } else {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button_gray)
                }
            }
        })
        binding.edtRegisterPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtRegisterName.text!!.isNotEmpty()
                    && binding.edtRegisterEmail.text!!.isNotEmpty()
                    && binding.edtRegisterPassword.text!!.isNotEmpty()
                    && binding.checkboxRegister.isChecked
                ) {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button)
                } else {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button_gray)
                }
            }
        })
        binding.checkboxRegister.setOnCheckedChangeListener { _, _ ->
            if (binding.edtRegisterName.text!!.isNotEmpty()
                && binding.edtRegisterEmail.text!!.isNotEmpty()
                && binding.edtRegisterPassword.text!!.isNotEmpty()
                && binding.checkboxRegister.isChecked
            ) {
                binding.btnRegister.setBackgroundResource(R.drawable.custom_button)
            } else {
                binding.btnRegister.setBackgroundResource(R.drawable.custom_button_gray)
            }
        }
    }
}