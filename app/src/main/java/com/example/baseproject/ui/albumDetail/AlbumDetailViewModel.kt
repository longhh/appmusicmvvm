package com.example.baseproject.ui.albumDetail

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.core.base.BaseViewModel
import com.example.core.model.Album
import com.example.core.model.SongItem
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AlbumDetailViewModel @Inject constructor(
    private val albumDetailRepository: AlbumDetailRepository,
    private val savedStateHandle: SavedStateHandle
) :
    BaseViewModel() {
    val listSongAlbum = MutableLiveData<ArrayList<SongItem>>()
    val imageAlbumLive = MutableLiveData<String>()
    val nameAlbumLive = MutableLiveData<String>()
    val progressBar = MutableLiveData<Boolean>()

    private val arrayListSongAlbum: ArrayList<SongItem> = ArrayList()
    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private var nameArtist: String? = null

    init {
        getSongAlbum()
    }

    private fun getSongAlbum() {
        CoroutineScope(Dispatchers.IO).launch {
            progressBar.postValue(true)
            try {
                val responseSong = albumDetailRepository.getSong()
                val responseArtist = albumDetailRepository.getArtist()
                val album: Album = savedStateHandle.get(Constants.Bundle.ALBUM)!!

                for (i in responseSong.indices) {
                    if (album.id != null) {
                        if (album.id == responseSong[i].albumId) {
                            val nameSong: String? = responseSong[i].title
                            val imageSong: String? = responseSong[i].image
                            val sourceSong: String? = responseSong[i].source
                            val artistId: Int? = responseSong[i].artistId
                            val durationSong: Int? = responseSong[i].duration
                            for (j in responseArtist.indices) {
                                if (artistId == responseArtist[j].id) {
                                    nameArtist = responseArtist[j].name
                                }
                            }
                            arrayListSongAlbum.add(SongItem(i, nameSong, imageSong, sourceSong, nameArtist, durationSong))
                        }
                    }
                }

                listSongAlbum.postValue(arrayListSongAlbum)
                imageAlbumLive.postValue(album.image!!)
                nameAlbumLive.postValue(album.name!!)
                progressBar.postValue(false)
            } catch (e: Exception) {
                Log.getStackTraceString(e)
                listSongAlbum.postValue(ArrayList())
            }
        }
    }

    fun addFavourite(songItem: SongItem) {
        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Firebase.REFERENCE_FAVOURITE)
                .child(firebaseUser.uid)
                .push()
                .setValue(songItem)
        }
    }
}
